<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_history', function (Blueprint $table) {
            $table->id();
            $table->foreignId('stock_id')->constrained('stocks');
            $table->integer('previous_stock')->default(0);
            $table->double('buying_price', 10, 2)->default(0);
            $table->double('selling_price', 10, 2)->default(0);
            $table->double('average_price', 10, 2)->default(0);
            $table->integer('reorder_level')->default(0);
            $table->timestamp('last_tran_date')->useCurrent();
            $table->timestamp('last_update')->useCurrent();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_history');
    }
}
