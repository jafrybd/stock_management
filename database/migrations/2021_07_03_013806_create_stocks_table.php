<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products');
            $table->integer('current_stock')->default(0);
            $table->double('buying_price', 10, 2)->default(0);
            $table->double('selling_price', 10, 2)->default(0);
            $table->double('average_price', 10, 2)->default(0);
            $table->integer('reorder_level')->default(0);
            $table->timestamp('last_tran_date')->useCurrent();
            $table->integer('store_id')->default(1);
            $table->boolean('status')->default(1);
            $table->integer('created_by');
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}
