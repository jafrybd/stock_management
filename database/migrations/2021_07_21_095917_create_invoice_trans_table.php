<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_trans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('customer_id')->nullable()->constrained('customers');
            $table->foreignId('supplier_id')->nullable()->constrained('suppliers');
            $table->string('inv_no');
            $table->string('tran_type');
            $table->double('sub_total', 13, 2);
            $table->double('due', 13, 2);
            $table->double('paid', 13, 2);
            $table->double('discount', 13, 2);
            $table->double('total', 13, 2);
            $table->integer('status')->default(1); // 1 = yes, 0 = no, 2 = precessing
            $table->boolean('is_advance_sales')->default(0);
            $table->integer('store_id')->default(1);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_trans');
    }
}
