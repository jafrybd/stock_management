<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_head_id')->constrained('product_heads');
            $table->foreignId('product_category_id')->nullable()->constrained('product_categories');
            $table->foreignId('product_type_id')->nullable()->constrained('product_types');
            $table->foreignId('product_size_id')->nullable()->constrained('product_sizes');
            $table->foreignId('product_group_id')->nullable()->constrained('product_groups');
            // $table->integer('product_head_id');
            // $table->integer('product_category_id')->default(0);
            // $table->integer('product_type_id')->default(0);
            // $table->integer('product_size_id')->default(0);
            // $table->integer('product_group_id')->default(0);
            $table->string('quantity')->default(0);
            $table->string('quatity_value')->default(0);
            $table->integer('store_id')->default(1);
            $table->boolean('status')->default(1);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
