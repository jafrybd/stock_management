<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockOpeningEndingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opening_ending_records', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')->constrained('products');
            $table->double('opening_amount', 10, 2)->default(0);
            $table->double('sales_amount', 10, 2)->default(0);
            $table->double('sales_return_amount', 10, 2)->default(0);
            $table->double('supplier_receive_amount', 10, 2)->default(0);
            $table->double('supplier_return_amount', 10, 2)->default(0);
            $table->double('ending_stock', 10, 2)->default(0);
            $table->integer('store_id')->default('1');
            $table->date('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opening_ending_records');
    }
}
