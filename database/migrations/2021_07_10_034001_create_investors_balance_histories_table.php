<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestorsBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investors_balance_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('investor_id')->constrained('investors');
            $table->double('current_balance', 10, 2)->default(0);
            $table->double('previous_balance', 10, 2)->default(0);
            $table->timestamp('last_update')->useCurrent();
            $table->integer('updated_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investors_balance_histories');
    }
}
