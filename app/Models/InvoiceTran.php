<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceTran extends Model
{
    use HasFactory;
    //protected $fillable = ['customer_id','supplier_id'];

    public function customer()
    {
    	return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function supplier()
    {
    	return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function invoiceTranItem()
    {
    	return $this->hasMany(InvoiceTranItem::class);
    }
}
