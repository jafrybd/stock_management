<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    public function customerHistory()
    {
    	return $this->hasMany(CustomerBalanceHistory::class);
    }

    public function invoiceTran()
    {
    	return $this->hasMany(InvoiceTran::class);
    }

    public function banks()
    {
    	return $this->hasMany(Banks::class);
    }

    
}
