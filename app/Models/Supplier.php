<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;
    protected $fillable = ['name','company_name','contact_no','address','store_id','status','created_by','created_at','updated_at'];


    public function supplierHistory()
    {
    	return $this->hasMany(SupplierBalanceHistory::class);
    }

    public function invoiceTran()
    {
    	return $this->hasMany(InvoiceTran::class);
    }

    public function banks()
    {
    	return $this->hasMany(Banks::class);
    }
}
