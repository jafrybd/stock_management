<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneralExpenseMonth extends Model
{
    use HasFactory;

    public function generalExpense()
    {
    	return $this->belongsTo(GeneralExpense::class, 'general_expense_id');
    }
}
