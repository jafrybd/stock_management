<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;

    public function partnersHistory()
    {
    	return $this->hasMany(PartnersBalanceHistory::class);
    }

    public function banks()
    {
    	return $this->hasMany(Banks::class);
    }
}
