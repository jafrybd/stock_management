<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    use HasFactory;
    protected $fillable = ['group_name','store_id','status','created_by','created_at','updated_at'];


    public function product()
    {
    	return $this->hasMany(Products::class);
    }
}
