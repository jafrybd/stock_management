<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    public function stocks()
    {
    	return $this->hasOne(Shops::class);
    }

    public function carts()
    {
    	return $this->hasMany(Carts::class);
    }

    public function invoiceTranItem()
    {
    	return $this->hasMany(InvoiceTranItem::class);
    }

    public function product_heads()
    {
    	return $this->belongsTo(ProductHead::class, 'product_head_id');
    }

    public function product_categories()
    {
    	return $this->belongsTo(ProductCategory::class, 'product_category_id');
    }

    public function product_types()
    {
    	return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function product_sizes()
    {
    	return $this->belongsTo(ProductSize::class, 'product_size_id');
    }

    public function product_groups()
    {
    	return $this->belongsTo(ProductGroup::class, 'product_group_id');
    }

    public function stock_opening_ending_records()
    {
    	return $this->hasMany(StockOpeningEndingRecords::class);
    }
}
