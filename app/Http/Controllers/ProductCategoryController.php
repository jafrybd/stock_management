<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\ProductCategory;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product_categories = ProductCategory::where('status', 1)->get();
        
        return view('product_category.index', compact('product_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        if (ProductCategory::where([['title', '=', $request->input('title')],['status','=',1]])->first()) {
            return redirect()->route('product-category.index')->with('error', 'Product Category Already Exists');
         }

        $product_category = new ProductCategory();
        $product_category->title = $request->title;                    
        $product_category->created_by = Auth::user()->id;                   
        $product_category->save();

        if ($product_category) {
            return redirect()->route('product-category.index')->with('success', 'Product Category Added');
        }
        // else{
        //     return redirect()->route('product-category.index')->with('error', 'Product  Category Exists');
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function show(ProductCategory $productCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product_category = ProductCategory::findorFail($id);
        
        return view('product_category.edit', compact('product_category', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        $product_category = ProductCategory::findorFail($id);
        if($product_category->title != $request->title){

        
             $existingData = ProductCategory::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first();

            
            if (!empty($existingData) && $existingData->id  != $product_category->id) {
                return redirect()->route('product-category.index')->with('error', 'Product Category Already Exists');
            }


            $product_category->title = $request->title;
            $product_category->save();
           
            if ($product_category) {
                return redirect()->route('product-category.index')->with('success', 'Product Category Updated');
            }
        }
        else{
            return redirect()->route('product-category.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductCategory  $productCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $product_category = ProductCategory::findorFail($id);

        try {
            $exit_product_list = Products::where([['status', '=', 1], ['product_category_id', $id]])->get();
            if (count($exit_product_list) > 0) {
                return redirect()->route('product-category.index')->with('error', 'You can\'t Delete this Product Category, It has Product.');
            }
        } catch (\Throwable $th) {
            return redirect()->route('product-category.index')->with('error', 'You can\'t Delete this Product Category, It has Product.');
        }

        $product_category->status =0;
        

        $product_category->save();
        return redirect()->route('product-category.index')->with('success', 'Product Category Delete');
    }
}
