<?php

namespace App\Http\Controllers;

use App\Models\Partner;
use Illuminate\Http\Request;
use App\Models\PartnersBalanceHistory;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use \PDF;

class PartnerController extends Controller
{

    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partner_list = Partner::where('status', 1)->get();
        return view('partner.index', compact('partner_list'));
    }

    
     // Generate PDF
     public function createPDF() {
       
        // retreive all records from db
        $partner = Partner::where('status', 1)->get();
       
        
        // share data to view
        view()->share('partner',$partner);
        $pdf = PDF::loadView('partner.pdf_view', $partner);
  
        // download PDF file with download method
        return $pdf->download('pdf_file.pdf');
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11',
            'percentile' => 'required|numeric|min:1|max:99',
        ]);

        if (Partner::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first()) {
            return redirect()->route('partner.index')->with('error', 'Partner Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $partner = new Partner();
        $partner->name = $request->name;
        $partner->contact_no = $request->phone;
        $partner->percentage = $request->percentile;
        $partner->address =  empty($request->address) ? "" : $request->address;
        $partner->created_by = Auth::user()->id;
        $partner->updated_by = Auth::user()->id;

        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/partners'), $imageName);
            $partner->image = $imageName;
        }

        $partner->save();

        if ($partner) {
            return redirect()->route('partner.index')->with('success', 'Partner ' . $request->input('name') . ' (' . $request->input('phone') . ') Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner_info = Partner::findorFail($id);
        return view('partner.edit', compact('partner_info', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11',
            'percentile' => 'required|numeric|min:1|max:99',
        ]);

        $partner_info = Partner::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('partner.index')->with('error', 'Partner Unknown');
        }

        $exitPartnerDetails = Partner::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first();

        if (!empty($exitPartnerDetails) && $exitPartnerDetails->id  != $partner_info->id) {
            return redirect()->route('partner.index')->with('error', 'Partner Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $partner_info->name = $request->name;
        $partner_info->contact_no = $request->phone;
        $partner_info->address =  empty($request->address) ? "" : $request->address;
        $partner_info->percentage = $request->percentile;
        $partner_info->created_by = Auth::user()->id;
        $partner_info->updated_by = Auth::user()->id;


        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/partners'), $imageName);
            $partner_info->image = $imageName;
        }


        $partner_info->save();

        if ($partner_info) {
            return redirect()->route('partner.index')->with('success', 'Partner info Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partner_info = Partner::findorFail($id);
        $partner_info->status = 0;
        $partner_info->save();

        return redirect()->route('partner.index')->with('success', 'partner ' . $partner_info->name . ' ( ' . $partner_info->contact_no . ' ) Deleted');
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function moneyUpdate(Request $request)
    {

        $validatedata = $request->validate([
            'money' => 'required|numeric|gt:0'
        ]);
        
        $partner = Partner::where([ ['id', '=', $request->id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
        $previous_balance = $partner[0]->balance;
       
            if ($request->operator == "add") {
               
                $partner[0]->balance +=   $request->money;
               
                if ($partner[0]->balance < 0) {
                    return redirect()->route('partner.index')->with('error', 'Balance  should not be negative');
                }
                
            } elseif ($request->operator == "subtract") {
                $partner[0]->balance -=   $request->money;
                
                if ($partner[0]->balance < 0) {
                    return redirect()->route('partner.index')->with('error', 'Balance  should not be negative');
                }
            } else {
                return redirect()->route('partner.index')->with('error', 'Unknown Request.');
            }

        $partner[0]->save();

        $partnerHistory = new PartnersBalanceHistory();
        $partnerHistory->partner_id = $partner[0]->id;
        $partnerHistory->current_balance = $partner[0]->balance;
        $partnerHistory->previous_balance = $previous_balance;
        $partnerHistory->last_update =Carbon::now()->toDateTimeString();
        $partnerHistory->updated_by = Auth::user()->id;
        $partnerHistory->save();

        // Journal
        $this->common_class_obj->generateJournalFromPartnerOrInvestor("partners_balance_histories", $request->money, $partnerHistory->id, $request->operator);
        return redirect()->route('partner.index')->with('success', 'Balance has been updated');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function balanceHistory($id)
    {
       
        $partner_balance_history = PartnersBalanceHistory::where('partner_id', '=', $id)->orderBy('id', 'DESC')->get();

        if(count($partner_balance_history)<1){
            return redirect()->route('partner.index')->with('error', 'No Data Exists');
        }

        $partner_name = $partner_balance_history[0]->partner->name;
        $current_balance = $partner_balance_history[0]->partner->balance;
        
        
        return view('partner.partnerBalanceHistory', compact('partner_balance_history', 'id','partner_name','current_balance'));
    }

}
