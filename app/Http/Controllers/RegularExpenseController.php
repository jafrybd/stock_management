<?php

namespace App\Http\Controllers;

use App\Models\RegularExpense;
use App\Http\Controllers\CommonController;
use App\Models\Journal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\DB as FacadesDB;

class RegularExpenseController extends Controller
{

    private $common_class_obj = "";

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $expenses = RegularExpense::where('status', 1)->orderBy('month', 'ASC')->get();
        $months = $this->common_class_obj->getMonthsName();
        return view('regular-expense.index', compact('expenses', 'months'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

        $m = date('m', strtotime($request->date));
        $y = date('Y', strtotime($request->date));
        $month = (int)$m;
        $year = (int)$y;

        $journals = array();

        $expense = new RegularExpense();
        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->date = $request->date;
        $expense->month = $month - 1;
        $expense->year = $year;
        $expense->created_by = Auth::user()->id;
        $expense->updated_by = Auth::user()->id;
        $expense->save();


        array_push($journals, array(
            "name" => "Expense",
            "type" => "Dr",
            "description" =>  "Regular Expense for " .  $request->title,
            "tran_type" => "expense",
            "tran_id" => $expense->id,
            "amount" => $request->amount,
            "created_at" => $$request->date,
            "updated_at" => $request->date
        ));

        array_push($journals, array(
            "name" => "Cash",
            "type" => "Cr",
            "description" =>  "Regular Expense for " .  $request->title,
            "tran_type" => "expense",
            "tran_id" => $expense->id,
            "amount" => $request->amount,
            "created_at" => $$request->date,
            "updated_at" => $request->date
        ));


        if ($expense) {
            $this->common_class_obj->saveJournal($journals);
            return redirect()->route('regular-expense.index')->with('success', 'Expense has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $expense_details = RegularExpense::findorFail($id);
        return view('regular-expense.edit', compact('expense_details', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $validatedata = $request->validate([
            'title' => 'required|max:255|min:3',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

        $expense = RegularExpense::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('regular-expense.index')->with('error', 'Expense Unknown');
        }

        $m = date('m', strtotime($request->date));
        $y = date('Y', strtotime($request->date));
        $month = (int)$m;
        $year = (int)$y;

        $expense->title = $request->title;
        $expense->amount = $request->amount;
        $expense->date = $request->date;
        $expense->month = $month - 1;
        $expense->year = $year;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        $list = Journal::where([['status', 1], ["tran_type", '=', 'expense'], ["tran_id", '=', $req_id]])
            ->get();

        for ($i = 0; $i < count($list); $i++) {

            $list[$i]->description =  "Regular Expense for " .  $request->title;
            $list[$i]->amount = $request->amount;
            $list[$i]->created_at = $request->date;
            $list[$i]->updated_at = $request->date;
            $list[$i]->save();
        }

        if ($expense) {
            return redirect()->route('regular-expense.index')->with('success', 'Expense Details Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $expense = RegularExpense::findorFail($id);

        $expense->status = 0;
        $expense->updated_by = Auth::user()->id;
        $expense->save();

        $list = Journal::where([['status', 1], ["tran_type", '=', 'expense'], ["tran_id", '=', $id]])
            ->get();

        for ($i = 0; $i < count($list); $i++) {
            $list[$i]->status = 0;
            $list[$i]->save();
        }

        return redirect()->route('regular-expense.index')->with('success', 'Expense has been Deleted');
    }


    /**
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {

        $filter_month = $request->filter_month;
        $filter_year = $request->filter_year;
        //$product_category_id = $request->product_category_id == null ? 0 : $request->product_category_id;

        try {
            $filter_month = (int)$filter_month;
            $filter_year = (int)$filter_year;
        } catch (\Throwable $th) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Request Data Format Error")));
        }

        if ($filter_month != null && $filter_year == null) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "You must select a Year")));
        }

        // return new JsonResponse(($filter_year));

        $query = FacadesDB::table('regular_expenses');
        $query->where('status', '=',  1);

        if ($filter_month != null) {
            $query->where('month', '=',  $filter_month);
        }
        if ($filter_year != null) {
            $query->where('year', '=',  $filter_year);
        }


        try {
            $expenses = $query->orderBy('month', 'ASC')->get();
        } catch (\Throwable $th) {
            $json = json_encode(array("success" => true, "message" => $th,));
            return new JsonResponse($json);
        }

        //return new JsonResponse(($expenses));

        json_encode($expenses);

        return view('regular-expense.regular_expense_table', compact('expenses'));
    }
}
