<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\Customer;
use App\Models\CustomerBalanceHistory;
use App\Models\Supplier;
use App\Models\SupplierBalanceHistory;
use App\Models\Partner;
use App\Models\PartnersBalanceHistory;
use App\Models\Investor;
use App\Models\InvestorsBalanceHistory;
use App\Http\Controllers\CommonController;
use App\Models\Journal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BanksController extends Controller
{

    private $common_class_obj;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banksData = Banks::where('status', 1)->orderBy('id', 'DESC')->get();

        return view('banks.index', compact('banksData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('banks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'user_type' => 'required',
            'user_name' => 'required|numeric',
            'cash_type' => 'required|numeric',
            'bank_name' => 'required|max:30|min:1',
            'check_no' => 'required|max:30|min:1',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

        $bankData = new Banks();
        $bankData->user_type = $request->user_type;
        $journals = array();
        $description = ""; 

        if ($request->user_type == "Customer") {
            $bankData->customer_id = $request->user_name;
        } else if ($request->user_type == "Supplier") {
            $bankData->supplier_id = $request->user_name;
        } else if ($request->user_type == "Partner") {
            $bankData->partner_id = $request->user_name;
        } else if ($request->user_type == "Investor") {
            $bankData->investor_id = $request->user_name;
        }

        $bankData->cash_type = $request->cash_type;
        $bankData->bank_name = $request->bank_name;
        $bankData->check_no = $request->check_no;
        $bankData->amount = $request->amount;
        $bankData->date = $request->date;
        $bankData->created_by = Auth::user()->id;
        $bankData->updated_by = Auth::user()->id;
        $bankData->save();

        /// User End

        /// Payable
        if ($bankData->cash_type == 1) {
            if ($request->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();
                $last_payable = $customer_info->payable;
                $last_receivable = $customer_info->receivable;
                

                if ($customer_info->payable > 0) {
                    if ($request->amount >= $customer_info->payable) {
                        $new_amount = $request->amount - $customer_info->payable;
                        if ($new_amount >= 0) {
                            $customer_info->payable = 0;
                            $customer_info->receivable += $new_amount;
                        }
                    } else {
                        $customer_info->payable -= $request->amount;
                    }
                } else {
                    $customer_info->receivable += $request->amount;
                }
                $customer_info->save();


                /// Customer Balance History

                $customerHistory = new CustomerBalanceHistory();
                $customerHistory->customer_id = $bankData->customer_id;
                $customerHistory->last_payable = $last_payable;
                $customerHistory->current_payable = $customer_info->payable;
                $customerHistory->last_receivable = $last_receivable;
                $customerHistory->current_receivable =  $customer_info->receivable;
                $customerHistory->amount =  $request->amount;
                $customerHistory->transaction_type =  0;
                $customerHistory->reason =  "Bank";
                $customerHistory->last_update =Carbon::now()->toDateTimeString();
                $customerHistory->updated_by = Auth::user()->id;
                
                $customerHistory->save();


            } else if ($request->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();
                $last_payable = $supplier_info->payable;
                $last_receivable = $supplier_info->receivable;

                if ($supplier_info->payable > 0) {
                    if ($request->amount >= $supplier_info->payable) {
                        $new_amount = $request->amount - $supplier_info->payable;
                        if ($new_amount >= 0) {
                            $supplier_info->payable = 0;
                            $supplier_info->receivable += $new_amount;
                        }
                    } else {
                        $supplier_info->payable -= $request->amount;
                    }
                } else {
                    $supplier_info->receivable += $request->amount;
                }
                $supplier_info->save();

                // Balance History

                $supplierHistory = new SupplierBalanceHistory();
                $supplierHistory->supplier_id = $bankData->supplier_id;
                $supplierHistory->last_payable = $last_payable;
                $supplierHistory->current_payable = $supplier_info->payable;
                $supplierHistory->last_receivable = $last_receivable;
                $supplierHistory->current_receivable =  $supplier_info->receivable;
                $supplierHistory->amount =  $request->amount;
                $supplierHistory->transaction_type =  0;
                $supplierHistory->reason =  "Bank";
                $supplierHistory->last_update = Carbon::now()->toDateTimeString();
                $supplierHistory->updated_by = Auth::user()->id;
                $supplierHistory->save();

            } else if ($request->user_type == "Partner") {

                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $partner[0]->balance;

                $partner[0]->balance +=   $request->amount;

                if ($partner[0]->balance < 0) {
                }
                $partner[0]->save();

                /// Balanace History

                $partnerHistory = new PartnersBalanceHistory();
                $partnerHistory->partner_id = $bankData->partner_id;
                $partnerHistory->current_balance = $partner[0]->balance;
                $partnerHistory->previous_balance = $previous_balance;
                $partnerHistory->last_update =Carbon::now()->toDateTimeString();
                $partnerHistory->updated_by = Auth::user()->id;
                $partnerHistory->save();

            } else if ($request->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $investor[0]->balance;

                $investor[0]->balance +=   $request->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();

                // Balance History
                $investorHistory = new InvestorsBalanceHistory();
                $investorHistory->investor_id = $bankData->investor_id;
                $investorHistory->current_balance = $investor[0]->balance;
                $investorHistory->previous_balance = $previous_balance;
                $investorHistory->last_update =Carbon::now()->toDateTimeString();
                $investorHistory->updated_by = Auth::user()->id;
                $investorHistory->save();
            }

            array_push($journals, array(
                "name" => "Account Payable",
                "type" => "Dr",
                "description" =>  "Receive From ".$request->user_type,
                "tran_type" => $request->user_type."sBalanceHistory",
                "tran_id" => $bankData->id,
                "amount" => $request->amount
            ));

            array_push($journals, array(
                "name" => "Bank",
                "type" => "Cr",
                "tran_type" => $request->user_type."sBalanceHistory",
                "tran_id" => $bankData->id,
                "amount" => $request->amount
            ));
        }
        /// Receivable
        else if ($bankData->cash_type == 2) {
            if ($request->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();
                $last_payable = $customer_info->payable;
                $last_receivable = $customer_info->receivable;


                if ($customer_info->receivable > 0) {

                    if ($request->amount >= $customer_info->receivable) {
                        $new_amount = $request->amount - $customer_info->receivable;
                        if ($new_amount >= 0) {
                            $customer_info->receivable = 0;
                            $customer_info->payable += $new_amount;
                        }
                    } else {
                        $customer_info->receivable -= $request->amount;
                    }
                } else {
                    $customer_info->payable += $request->amount;
                }
                $customer_info->save();


                /// Customer Balance History

                $customerHistory = new CustomerBalanceHistory();
                $customerHistory->customer_id = $bankData->customer_id;
                $customerHistory->last_payable = $last_payable;
                $customerHistory->current_payable = $customer_info->payable;
                $customerHistory->last_receivable = $last_receivable;
                $customerHistory->current_receivable =  $customer_info->receivable;
                $customerHistory->amount =  $request->amount;
                $customerHistory->transaction_type =  0;
                $customerHistory->reason =  "Bank";
                $customerHistory->last_update =Carbon::now()->toDateTimeString();
                $customerHistory->updated_by = Auth::user()->id;
                
                $customerHistory->save();


            } else if ($request->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();
                $last_payable = $supplier_info->payable;
                $last_receivable = $supplier_info->receivable;


                if ($supplier_info->receivable > 0) {

                    if ($request->amount >= $supplier_info->receivable) {
                        $new_amount = $request->amount - $supplier_info->receivable;
                        if ($new_amount >= 0) {
                            $supplier_info->receivable = 0;
                            $supplier_info->payable += $new_amount;
                        }
                    } else {
                        $supplier_info->receivable -= $request->amount;
                    }
                } else {
                    $supplier_info->payable += $request->amount;
                }
                $supplier_info->save();

                // Balance History

                $supplierHistory = new SupplierBalanceHistory();
                $supplierHistory->supplier_id = $bankData->supplier_id;
                $supplierHistory->last_payable = $last_payable;
                $supplierHistory->current_payable = $supplier_info->payable;
                $supplierHistory->last_receivable = $last_receivable;
                $supplierHistory->current_receivable =  $supplier_info->receivable;
                $supplierHistory->amount =  $request->amount;
                $supplierHistory->transaction_type =  1;
                $supplierHistory->reason =  "Bank";
                $supplierHistory->last_update = Carbon::now()->toDateTimeString();
                $supplierHistory->updated_by = Auth::user()->id;
                $supplierHistory->save();

            } else if ($request->user_type == "Partner") {
                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $partner[0]->balance;

                $partner[0]->balance -=   $request->amount;

                if ($partner[0]->balance < 0) {
                    //return redirect()->route('partner.index')->with('error', 'Balance  should not be negative');
                }
                $partner[0]->save();

                /// Balanace History

                $partnerHistory = new PartnersBalanceHistory();
                $partnerHistory->partner_id = $bankData->partner_id;
                $partnerHistory->current_balance = $partner[0]->balance;
                $partnerHistory->previous_balance = $previous_balance;
                $partnerHistory->last_update =Carbon::now()->toDateTimeString();
                $partnerHistory->updated_by = Auth::user()->id;
                $partnerHistory->save();
                
            } else if ($request->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $investor[0]->balance;

                $investor[0]->balance -=   $request->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();

                 // Balance History
                 $investorHistory = new InvestorsBalanceHistory();
                 $investorHistory->investor_id = $bankData->investor_id;
                 $investorHistory->current_balance = $investor[0]->balance;
                 $investorHistory->previous_balance = $previous_balance;
                 $investorHistory->last_update =Carbon::now()->toDateTimeString();
                 $investorHistory->updated_by = Auth::user()->id;
                 $investorHistory->save();
            }

            array_push($journals, array(
                "name" => "Bank",
                "type" => "Dr",
                "description" =>  "Receive From Supplier".$request->user_type,
                "tran_type" => $request->user_type."sBalanceHistory",
                "tran_id" => $bankData->id,
                "amount" => $request->amount
            ));

            array_push($journals, array(
                "name" => "Account Receivable",
                "type" => "Cr",
                "tran_type" => $request->user_type."sBalanceHistory",
                "tran_id" => $bankData->id,
                "amount" => $request->amount
            ));
        }


        if ($bankData) {
            $this->common_class_obj->saveJournal($journals);
            return redirect()->route('bank.index')->with('success', 'Bank data has been added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banks  $banks
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $bankData = Banks::findorFail($id);
        return view('banks.details', compact('bankData'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banks  $banks
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $bankData = Banks::findorFail($id);

        return view('banks.edit', compact('bankData', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banks  $banks
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'user_type' => 'required',
            'user_name' => 'required|numeric',
            'cash_type' => 'required|numeric',
            'bank_name' => 'required|max:30|min:1',
            'check_no' => 'required|max:30|min:1',
            'amount' => 'required|min:1|numeric',
            'date' => 'required',
        ]);

        $bankData = Banks::findorFail($id);

        if ($bankData->user_type != $request->user_type) {
            if ($bankData->customer_id != null) {
                $bankData->customer_id = null;
            } else if ($bankData->supplier_id != null) {
                $bankData->supplier_id = null;
            } else if ($bankData->partner_id != null) {
                $bankData->partner_id = null;
            } else if ($bankData->investor_id != null) {
                $bankData->investor_id = null;
            }
        }

        $bankData->user_type = $request->user_type;

        if ($request->user_type == "Customer") {
            $bankData->customer_id = $request->user_name;
        } else if ($request->user_type == "Supplier") {
            $bankData->supplier_id = $request->user_name;
        } else if ($request->user_type == "Partner") {
            $bankData->partner_id = $request->user_name;
        } else if ($request->user_type == "Investor") {
            $bankData->investor_id = $request->user_name;
        }

        $bankData->cash_type = $request->cash_type;
        $bankData->bank_name = $request->bank_name;
        $bankData->check_no = $request->check_no;
        $bankData->amount = $request->amount;
        $bankData->date = $request->date;
        $bankData->updated_by = Auth::user()->id;
        $bankData->save();


        /// User End

        /// Payable
        if ($bankData->cash_type == 1) {
            if ($request->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();

                if ($customer_info->payable > 0) {
                    if ($request->amount >= $customer_info->payable) {
                        $new_amount = $request->amount - $customer_info->payable;
                        if ($new_amount >= 0) {
                            $customer_info->payable = 0;
                            $customer_info->receivable += $new_amount;
                        }
                    } else {
                        $customer_info->payable -= $request->amount;
                    }
                } else {
                    $customer_info->receivable = $request->amount;
                }
                $customer_info->save();
            } else if ($request->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();

                if ($supplier_info->payable > 0) {
                    if ($request->amount >= $supplier_info->payable) {
                        $new_amount = $request->amount - $supplier_info->payable;
                        if ($new_amount >= 0) {
                            $supplier_info->payable = 0;
                            $supplier_info->receivable += $new_amount;
                        }
                    } else {
                        $supplier_info->payable -= $request->amount;
                    }
                } else {
                    $supplier_info->receivable = $request->amount;
                }
                $supplier_info->save();
            } else if ($request->user_type == "Partner") {

                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $partner[0]->balance +=   $request->amount;

                if ($partner[0]->balance < 0) {
                }
                $partner[0]->save();
            } else if ($request->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $investor[0]->balance +=   $request->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();
            }
        }
        /// Receivable
        else if ($bankData->cash_type == 2) {
            if ($request->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();


                if ($customer_info->receivable > 0) {

                    if ($request->amount >= $customer_info->receivable) {
                        $new_amount = $request->amount - $customer_info->receivable;
                        if ($new_amount >= 0) {
                            $customer_info->receivable = 0;
                            $customer_info->payable += $new_amount;
                        }
                    } else {
                        $customer_info->receivable -= $request->amount;
                    }
                } else {
                    $customer_info->payable = $request->amount;
                }
                $customer_info->save();
            } else if ($request->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();


                if ($supplier_info->receivable > 0) {

                    if ($request->amount >= $supplier_info->receivable) {
                        $new_amount = $request->amount - $supplier_info->receivable;
                        if ($new_amount >= 0) {
                            $supplier_info->receivable = 0;
                            $supplier_info->payable += $new_amount;
                        }
                    } else {
                        $supplier_info->receivable -= $request->amount;
                    }
                } else {
                    $supplier_info->payable = $request->amount;
                }
                $supplier_info->save();
            } else if ($request->user_type == "Partner") {
                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();

                $partner[0]->balance -=   $request->amount;

                if ($partner[0]->balance < 0) {
                    //return redirect()->route('partner.index')->with('error', 'Balance  should not be negative');
                }
                $partner[0]->save();
            } else if ($request->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $investor[0]->balance -=   $request->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();
            }
        }


        if ($bankData) {
            return redirect()->route('bank.index')->with('success', 'Bank data has been updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banks  $banks
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $bankData = Banks::findorFail($id);

        $bankData->status = 0;
        $bankData->updated_by = Auth::user()->id;

        /// User End

        /// Payable
        // ulta ta hbe insert er
        if ($bankData->cash_type == 2) {
            if ($bankData->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();
                $last_payable = $customer_info->payable;
                $last_receivable = $customer_info->receivable;
                

                if ($customer_info->payable > 0) {
                    if ($bankData->amount >= $customer_info->payable) {
                        $new_amount = $bankData->amount - $customer_info->payable;
                        if ($new_amount >= 0) {
                            $customer_info->payable = 0;
                            $customer_info->receivable += $new_amount;
                        }
                    } else {
                        $customer_info->payable -= $bankData->amount;
                    }
                } else {
                    $customer_info->receivable += $bankData->amount;
                }
                $customer_info->save();


                /// Customer Balance History

                $customerHistory = new CustomerBalanceHistory();
                $customerHistory->customer_id = $bankData->customer_id;
                $customerHistory->last_payable = $last_payable;
                $customerHistory->current_payable = $customer_info->payable;
                $customerHistory->last_receivable = $last_receivable;
                $customerHistory->current_receivable =  $customer_info->receivable;
                $customerHistory->amount =  $bankData->amount;
                $customerHistory->transaction_type =  0;
                $customerHistory->reason =  "Bank";
                $customerHistory->last_update =Carbon::now()->toDateTimeString();
                $customerHistory->updated_by = Auth::user()->id;
                
                $customerHistory->save();


            } else if ($bankData->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();
                $last_payable = $supplier_info->payable;
                $last_receivable = $supplier_info->receivable;

                if ($supplier_info->payable > 0) {
                    if ($bankData->amount >= $supplier_info->payable) {
                        $new_amount = $bankData->amount - $supplier_info->payable;
                        if ($new_amount >= 0) {
                            $supplier_info->payable = 0;
                            $supplier_info->receivable += $new_amount;
                        }
                    } else {
                        $supplier_info->payable -= $bankData->amount;
                    }
                } else {
                    $supplier_info->receivable += $bankData->amount;
                }
                $supplier_info->save();

                // Balance History

                $supplierHistory = new SupplierBalanceHistory();
                $supplierHistory->supplier_id = $bankData->supplier_id;
                $supplierHistory->last_payable = $last_payable;
                $supplierHistory->current_payable = $supplier_info->payable;
                $supplierHistory->last_receivable = $last_receivable;
                $supplierHistory->current_receivable =  $supplier_info->receivable;
                $supplierHistory->amount =  $bankData->amount;
                $supplierHistory->transaction_type =  0;
                $supplierHistory->reason =  "Bank";
                $supplierHistory->last_update = Carbon::now()->toDateTimeString();
                $supplierHistory->updated_by = Auth::user()->id;
                $supplierHistory->save();

            } else if ($bankData->user_type == "Partner") {

                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $partner[0]->balance;

                $partner[0]->balance +=   $bankData->amount;

                if ($partner[0]->balance < 0) {
                }
                $partner[0]->save();

                /// Balanace History

                $partnerHistory = new PartnersBalanceHistory();
                $partnerHistory->partner_id = $bankData->partner_id;
                $partnerHistory->current_balance = $partner[0]->balance;
                $partnerHistory->previous_balance = $previous_balance;
                $partnerHistory->last_update =Carbon::now()->toDateTimeString();
                $partnerHistory->updated_by = Auth::user()->id;
                $partnerHistory->save();

            } else if ($bankData->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $investor[0]->balance;

                $investor[0]->balance +=   $bankData->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();

                // Balance History
                $investorHistory = new InvestorsBalanceHistory();
                $investorHistory->investor_id = $bankData->investor_id;
                $investorHistory->current_balance = $investor[0]->balance;
                $investorHistory->previous_balance = $previous_balance;
                $investorHistory->last_update =Carbon::now()->toDateTimeString();
                $investorHistory->updated_by = Auth::user()->id;
                $investorHistory->save();
            }

           
           
        }
        /// Receivable
        // ulta ta hbe insert er
        else if ($bankData->cash_type == 1) {
            if ($bankData->user_type == "Customer") {
                $customer_info = Customer::where([['id', '=', $bankData->customer_id], ['status', '=', 1]])->first();
                $last_payable = $customer_info->payable;
                $last_receivable = $customer_info->receivable;


                if ($customer_info->receivable > 0) {

                    if ($bankData->amount >= $customer_info->receivable) {
                        $new_amount = $bankData->amount - $customer_info->receivable;
                        if ($new_amount >= 0) {
                            $customer_info->receivable = 0;
                            $customer_info->payable += $new_amount;
                        }
                    } else {
                        $customer_info->receivable -= $bankData->amount;
                    }
                } else {
                    $customer_info->payable += $bankData->amount;
                }
                $customer_info->save();


                /// Customer Balance History

                $customerHistory = new CustomerBalanceHistory();
                $customerHistory->customer_id = $bankData->customer_id;
                $customerHistory->last_payable = $last_payable;
                $customerHistory->current_payable = $customer_info->payable;
                $customerHistory->last_receivable = $last_receivable;
                $customerHistory->current_receivable =  $customer_info->receivable;
                $customerHistory->amount =  $bankData->amount;
                $customerHistory->transaction_type =  0;
                $customerHistory->reason =  "Bank";
                $customerHistory->last_update =Carbon::now()->toDateTimeString();
                $customerHistory->updated_by = Auth::user()->id;
                
                $customerHistory->save();


            } else if ($bankData->user_type == "Supplier") {
                $supplier_info = Supplier::where([['id', '=', $bankData->supplier_id], ['status', '=', 1]])->first();
                $last_payable = $supplier_info->payable;
                $last_receivable = $supplier_info->receivable;


                if ($supplier_info->receivable > 0) {

                    if ($bankData->amount >= $supplier_info->receivable) {
                        $new_amount = $bankData->amount - $supplier_info->receivable;
                        if ($new_amount >= 0) {
                            $supplier_info->receivable = 0;
                            $supplier_info->payable += $new_amount;
                        }
                    } else {
                        $supplier_info->receivable -= $bankData->amount;
                    }
                } else {
                    $supplier_info->payable += $bankData->amount;
                }
                $supplier_info->save();

                // Balance History

                $supplierHistory = new SupplierBalanceHistory();
                $supplierHistory->supplier_id = $bankData->supplier_id;
                $supplierHistory->last_payable = $last_payable;
                $supplierHistory->current_payable = $supplier_info->payable;
                $supplierHistory->last_receivable = $last_receivable;
                $supplierHistory->current_receivable =  $supplier_info->receivable;
                $supplierHistory->amount =  $bankData->amount;
                $supplierHistory->transaction_type =  1;
                $supplierHistory->reason =  "Bank";
                $supplierHistory->last_update = Carbon::now()->toDateTimeString();
                $supplierHistory->updated_by = Auth::user()->id;
                $supplierHistory->save();

            } else if ($bankData->user_type == "Partner") {
                $partner = Partner::where([['id', '=', $bankData->partner_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $partner[0]->balance;

                $partner[0]->balance -=   $bankData->amount;

                if ($partner[0]->balance < 0) {
                    //return redirect()->route('partner.index')->with('error', 'Balance  should not be negative');
                }
                $partner[0]->save();

                /// Balanace History

                $partnerHistory = new PartnersBalanceHistory();
                $partnerHistory->partner_id = $bankData->partner_id;
                $partnerHistory->current_balance = $partner[0]->balance;
                $partnerHistory->previous_balance = $previous_balance;
                $partnerHistory->last_update =Carbon::now()->toDateTimeString();
                $partnerHistory->updated_by = Auth::user()->id;
                $partnerHistory->save();
                
            } else if ($bankData->user_type == "Investor") {


                $investor = Investor::where([['id', '=', $bankData->investor_id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
                $previous_balance = $investor[0]->balance;

                $investor[0]->balance -=   $bankData->amount;

                if ($investor[0]->balance < 0) {
                }
                $investor[0]->save();

                 // Balance History
                 $investorHistory = new InvestorsBalanceHistory();
                 $investorHistory->investor_id = $bankData->investor_id;
                 $investorHistory->current_balance = $investor[0]->balance;
                 $investorHistory->previous_balance = $previous_balance;
                 $investorHistory->last_update =Carbon::now()->toDateTimeString();
                 $investorHistory->updated_by = Auth::user()->id;
                 $investorHistory->save();
            }

            
        }

        $journalData = Journal::where([['tran_id', '=', $bankData->id], ['status', '=', 1]])->delete();
        $bankData->save();

        return redirect()->route('bank.index')->with('success', 'Bank Data has been Deleted');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getUserName(Request $request)
    {
        $user_type = $request->user_type;

        $response = array("success" => true, "isFindData" => false, "data" => "{}", "message" => "");

        if ($user_type == "Customer") {
            $customerList =  Customer::where([['status', 1]])->get();



            if (count($customerList) > 0) {
                $response['isFindData'] =  true;
                $response['data'] = $customerList;
            }
        } else if ($user_type == "Supplier") {
            $supplierList =  Supplier::where([['status', 1]])->get();



            if (count($supplierList) > 0) {
                $response['isFindData'] =  true;
                $response['data'] = $supplierList;
            }
        } else if ($user_type == "Partner") {
            $partnerList =  Partner::where([['status', 1]])->get();



            if (count($partnerList) > 0) {
                $response['isFindData'] =  true;
                $response['data'] = $partnerList;
            }
        } else if ($user_type == "Investor") {
            $investorList =  Investor::where([['status', 1]])->get();



            if (count($investorList) > 0) {
                $response['isFindData'] =  true;
                $response['data'] = $investorList;
            }
        }


        return new JsonResponse(json_encode($response));
    }
}
