<?php

namespace App\Http\Controllers;

use App\Models\Investor;
use App\Models\InvestorsBalanceHistory;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class InvestorController extends Controller
{

    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $investor_list = Investor::where('status', 1)->get();
        return view('investor.index', compact('investor_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11',
            'percentile' => 'required|numeric|min:1|max:99',
        ]);

        if (Investor::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first()) {
            return redirect()->route('investor.index')->with('error', 'Investor Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $investor = new Investor();
        $investor->name = $request->name;
        $investor->contact_no = $request->phone;
        $investor->percentage = $request->percentile;
        $investor->address =  empty($request->address) ? "" : $request->address;
        $investor->created_by = Auth::user()->id;
        $investor->updated_by = Auth::user()->id;

        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/investors'), $imageName);
            $investor->image = $imageName;
        }

        $investor->save();

        if ($investor) {
            return redirect()->route('investor.index')->with('success', 'Investor ' . $request->input('name') . ' (' . $request->input('phone') . ') Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function show(Investor $investor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $investor_info = Investor::findorFail($id);
        return view('investor.edit', compact('investor_info', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11',
            'percentile' => 'required|numeric|min:1|max:99',
        ]);

        $investor_info = Investor::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('investor.index')->with('error', 'Investor Unknown');
        }

        $exitInvestorDetails = Investor::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first();

        if (!empty($exitInvestorDetails) && $exitInvestorDetails->id  != $investor_info->id) {
            return redirect()->route('investor.index')->with('error', 'Investor Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $investor_info->name = $request->name;
        $investor_info->contact_no = $request->phone;
        $investor_info->address =  empty($request->address) ? "" : $request->address;
        $investor_info->percentage = $request->percentile;
        $investor_info->created_by = Auth::user()->id;
        $investor_info->updated_by = Auth::user()->id;


        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/investors'), $imageName);
            $investor_info->image = $imageName;
        }


        $investor_info->save();

        if ($investor_info) {
            return redirect()->route('investor.index')->with('success', 'Investor info Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Investor_info = Investor::findorFail($id);
        $Investor_info->status = 0;
        $Investor_info->save();

        return redirect()->route('investor.index')->with('success', 'investor ' . $Investor_info->name . ' ( ' . $Investor_info->contact_no . ' ) Deleted');
   
    }


    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function moneyUpdate(Request $request)
    {

        $validatedata = $request->validate([
            'money' => 'required|numeric|gt:0'
        ]);
        
        $investor = Investor::where([ ['id', '=', $request->id], ['status', '=', 1]])->orderBy('id', 'ASC')->get();
        $previous_balance = $investor[0]->balance;
       
            if ($request->operator == "add") {
               
                $investor[0]->balance +=   $request->money;
               
                if ($investor[0]->balance < 0) {
                    return redirect()->route('investor.index')->with('error', 'Balance  should not be negative');
                }
                
            } elseif ($request->operator == "subtract") {
                $investor[0]->balance -=   $request->money;
                
                if ($investor[0]->balance < 0) {
                    return redirect()->route('investor.index')->with('error', 'Balance  should not be negative');
                }
            }

        $investor[0]->save();

        $investorHistory = new InvestorsBalanceHistory();
        $investorHistory->investor_id = $investor[0]->id;
        $investorHistory->current_balance = $investor[0]->balance;
        $investorHistory->previous_balance = $previous_balance;
        $investorHistory->last_update =Carbon::now()->toDateTimeString();
        $investorHistory->updated_by = Auth::user()->id;
        $investorHistory->save();

        // Journal
        $this->common_class_obj->generateJournalFromPartnerOrInvestor("investors_balance_histories", $request->money, $investorHistory->id, $request->operator);

            return redirect()->route('investor.index')->with('success', 'Balance has been updated');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Investor  $investor
     * @return \Illuminate\Http\Response
     */
    public function balanceHistory($id)
    {
       
        $investor_balance_history = InvestorsBalanceHistory::where('investor_id', '=', $id)->orderBy('id', 'DESC')->get();

        if(count($investor_balance_history)<1){
            return redirect()->route('investor.index')->with('error', 'No Data Exists');
        }

        $investor_name = $investor_balance_history[0]->investor->name;
        $current_balance = $investor_balance_history[0]->investor->balance;
        
        
        return view('investor.investorBalanceHistory', compact('investor_balance_history', 'id','investor_name','current_balance'));
    }
}
