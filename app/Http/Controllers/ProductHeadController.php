<?php

namespace App\Http\Controllers;

use App\Models\ProductHead;
use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProductHeadController extends Controller
{
    private $common_class_obj = "";

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        // $journals = array();

        // array_push($journals, array(
        //     "name" => "CASH",
        //     "type" => "Dr",
        //     "tran_type" => "invoice",
        //     "tran_id" => 1,
        //     "amount" => 10,
        // ));

        // array_push($journals, array(
        //     "name" => "PRODUCT",
        //     "type" => "Cr",
        //     "tran_type" => "invoice",
        //     "tran_id" => 1,
        //     "amount" => 10,
        // ));

        // $this->common_class_obj->saveJournal($journals);

        $product_heads = ProductHead::where('status', 1)->get();
        return view('product_head.index', compact('product_heads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        if (ProductHead::where([['title', '=', $request->input('title')],['status','=',1]])->first()) {
            return redirect()->route('product-head.index')->with('error', 'Product  Head Exists');
         }

        $product_head = new ProductHead();
        $product_head->title = $request->title;
        $product_head->save();

        if ($product_head) {
            return redirect()->route('product-head.index')->with('success', 'Product Head Added');
        }
        // else{
        //     return redirect()->route('product-head.index')->with('error', 'Product Head Already Exists');
        // }

        // $product_head_exits = ProductHead::firstOrCreate(['title' => $request->title]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductHead  $productHead
     * @return \Illuminate\Http\Response
     */
    public function show(ProductHead $productHead)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductHead  $productHead
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product_head = ProductHead::findorFail($id);
        
        return view('product_head.edit', compact('product_head', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductHead  $productHead
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        $product_head = ProductHead::findorFail($id);
        if($product_head->title != $request->title){
            
            $existingData = ProductHead::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first();

            
            if (!empty($existingData) && $existingData->id  != $product_head->id) {
                return redirect()->route('product-head.index')->with('error', 'Product  Head already Exists');
            }

            $product_head->title = $request->title;
            $product_head->save();
            //dd($item);
            if ($product_head) {
                return redirect()->route('product-head.index')->with('success', 'Product Head Updated');
            }
        }
        else{
            return redirect()->route('product-head.index');
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductHead  $productHead
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
        $product_head = ProductHead::findorFail($id);

        try {
            $exit_product_list = Products::where([['status', '=', 1], ['product_head_id', $id]])->get();
            if (count($exit_product_list) > 0) {
                return redirect()->route('product-head.index')->with('error', 'You can\'t Delete this Product Head, It has Product.');
            }
        } catch (\Throwable $th) {
            return redirect()->route('product-head.index')->with('error', 'You can\'t Delete this Product Head, It has Product.');
        }
        
        $product_head->status =0;
        $product_head->save();
        return redirect()->route('product-head.index')->with('success', 'Product Head Delete');
    }


    public function modalEdit(Request $request)
    {
        //
        $id = $request->id;
        $title = $request->title;

        

        $product_head = ProductHead::where([['status', '=', 1], ['id', $request->id]])->get();
        

        if ( count($product_head) > 0) {
            $product_head_details = $product_head[0];
           
        } else {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Head")));
        }


        if($product_head_details->title != $title){
            
            $existingData = ProductHead::where([['title', '=', $title], ['status', '=', 1]])->first();

            
            if (!empty($existingData) && $existingData->id  != $id) {
                return new JsonResponse(json_encode(array("success" => false, "message" => " Product Head already exists")));
            }

            $product_head_details->title = $title;
            $product_head_details->save();

            $product_heads = ProductHead::where('status', 1)->get();
            //return new JsonResponse(json_encode(array("success" => true, "message" => "Product head updated.",$product_heads)));

            //Table reload
            $table = '<table class="table table-striped table-bordered nowrap" id="example">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Title</th>
                      <th scope="col">Action</th>
                      
                    </tr>
                  </thead>
                  <tbody>';


                  for ($i = 0; $i < count($product_heads); $i++) {
                    $table = $table . '<tr>
              <td> ' . ($i + 1) . '</td>
              <td >' . $product_heads[$i]->title . ' </td>
              <td >';
           
                    $table = $table . '<a href="/product-head/'.$product_heads[$i]->id.'/edit" type="button" class="btn btn-primary edit">Edit</a>';
                //     $table = $table . ' <a class="btn btn-primary" onclick="changeModalData( '.$product_heads[i].')" data-toggle="modal" data-target="#modal-part2">
                //     + Check Modal
                // </a>';

                $table = $table . ' <a class="btn btn-primary" onclick="changeModalData(\''.$product_heads[$i] .'\')" data-toggle="modal" data-target="#modal-part2">
                    + Check Modal
                </a>';

                // <a class="btn btn-primary" onclick="changeModalData('{" id":1,"title":"bangla="" 120","store_id":1,"status":1,
                //     "created_at":"2021-07-07t02:51:23.000000z","updated_at":"2021-07-09t18:41:02.000000z"}')"="" data-toggle="modal" data-target="#modal-part2">
                //     + Check Modal
                // </a>
                //     $table = $table . '<form method="POST"
                //     product-head/{product_head}
                //     action="/product-head/'.$product_heads[$i]->id.'"
                //     style="display:inline">
                //     {{ method_field('DELETE') }}
                //     {{ csrf_field() }}
                //     <button type="submit" class=" btn-sm btn-danger"
                //         onclick="return confirm('Confirm delete?')"><i
                //             class="fa fa-trash-o" aria-hidden="true"></i>Delete

                //     </button>
                // </form>';
             
              
              $table = $table . ' </td>
             
             
              </tr> ';
                }
        
                $table = $table . '</tbody></table>';

            //dd($item);
            
                $json = json_encode(array("success" => true, "message" => "Product head updated.","data" =>$table));
        return new JsonResponse($json);
            
        }
        else{
            return new JsonResponse(json_encode(array("success" => false, "message" => " not updated")));
        }

    }
}
