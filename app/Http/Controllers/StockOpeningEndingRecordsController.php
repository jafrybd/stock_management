<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\StockOpeningEndingRecords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StockOpeningEndingRecordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if ($request->search_date == null) {
            $date = date("Y-m-d");
        } else {
            $date = $request->search_date;
        }


        //  ------- system one start

        // $list = StockOpeningEndingRecords::where("date", $date)->get();

        // if (count($list) > 0) {

        //     $this->create_new_stockOpeningEndingRecords_from_date($date);
        //     $list = StockOpeningEndingRecords::where("date", $date)->get();

        // } else {

        //     $last_stock_records_date = DB::select('
        //         SELECT date  FROM `stock_opening_ending_records` where store_id = 1 and date < "' . $date . '" order by date DESC limit 1
        //         ');

        //     $last_invoice_date = DB::select('
        //         SELECT DISTINCT date(created_at) as date FROM `invoice_trans` where status = 1 and created_at < "' . $date . ' order by created_at DESC"
        //      ');

        //     $temp_date = "2021-08-01";

        //     if (count($last_stock_records_date) > 0 and count($last_invoice_date) > 0) {
        //         $temp_date = (date($last_stock_records_date[0]->date) < date($last_invoice_date[0]->date)) ?  $last_stock_records_date[0]->date : $last_invoice_date[0]->date;
        //     } else if (count($last_stock_records_date) > 0) {
        //         $temp_date = $last_stock_records_date[0]->date;
        //     } else if (count($last_invoice_date) > 0) {
        //         $temp_date = $last_invoice_date[0]->date;
        //     }

        //     $this->create_new_stockOpeningEndingRecords_from_date($temp_date);
        //     $list = StockOpeningEndingRecords::where("date", $date)->get();

        // }

        //  ------- system one end


        //  ------- system 2 start
        $last_stock_records_date = DB::select('
                SELECT date  FROM `stock_opening_ending_records` where store_id = 1 and date < "' . $date . '" order by date DESC limit 1
                ');

        $last_invoice_date = DB::select('
                SELECT DISTINCT date(created_at) as date FROM `invoice_trans` where status = 1 and created_at < "' . $date . ' order by created_at DESC"
             ');

        $temp_date = "2021-08-01";

        if (count($last_stock_records_date) > 0 and count($last_invoice_date) > 0) {
            $temp_date = (date($last_stock_records_date[0]->date) < date($last_invoice_date[0]->date)) ?  $last_stock_records_date[0]->date : $last_invoice_date[0]->date;
        } else if (count($last_stock_records_date) > 0) {
            $temp_date = $last_stock_records_date[0]->date;
        } else if (count($last_invoice_date) > 0) {
            $temp_date = $last_invoice_date[0]->date;
        }

        $this->create_new_stockOpeningEndingRecords_from_date($temp_date);
        $list = StockOpeningEndingRecords::where("date", $date)->get();

        //  ------- system 2 end
        // dump($list[0]->product->product_heads->title);
        // dump($list[0]->product->product_categories->title);
        // dump($list[0]->product->product_types->title);
        // dump($list[0]->product->product_sizes->title);
        // dump($list[0]->product->product_groups->title);
        // dd($list);
        return view('stock.stock_opening_ending_reports', compact('list', 'date'));
    }



    public function create_new_stockOpeningEndingRecords_from_date($start_date)
    {

        // first get all tran date, then get product date. and pass it for create records.
        $start_date = date($start_date);

        $invoice_date = DB::select('
            SELECT DISTINCT date(created_at) as date FROM `invoice_trans` where status = 1 and created_at >= "' . $start_date . '"
         ');

        for ($i = 0; $i < count($invoice_date); $i++) {
            $product_list = Products::where([['status', '=', 1], ['created_at', "<=", $invoice_date[$i]->date . " 23:59:59"]])->get();

            for ($j = 0; $j < count($product_list); $j++) {
                $this->create_new_or_update_stockOpeningEndingRecords_by_productId_and_date($product_list[$j]->id, $invoice_date[$i]->date);
            }
        }
    }


    public function create_new_or_update_stockOpeningEndingRecords_by_productId_and_date($product_id, $date)
    {

        $tempStockRecords =  StockOpeningEndingRecords::where([["date", $date], ["product_id", $product_id]])->get();

        if (count($tempStockRecords) > 0) {
            $stockOpeningEndingRecords = $tempStockRecords[0];
        } else {
            $stockOpeningEndingRecords = new StockOpeningEndingRecords();
        }


        $stockOpeningEndingRecords->product_id = $product_id;
        $stockOpeningEndingRecords->opening_amount = 0;
        $stockOpeningEndingRecords->date = $date;

        $last_stock_records_date = DB::select('
                SELECT *  FROM `stock_opening_ending_records` where store_id = 1 and product_id = ' . $product_id . ' and date < "' . $date . '" order by date DESC limit 1
                ');

        if (count($last_stock_records_date) > 0) {
            $stockOpeningEndingRecords->opening_amount = $last_stock_records_date[0]->ending_stock;
        }


        $stock_records_date = DB::select('
        SELECT 
            SUM(CASE WHEN tran_type = "sales to customer" then qty ELSE 0 END) as "sales",
            SUM(CASE WHEN tran_type = "Customer Return" then qty ELSE 0 END) as "customer_return",
            SUM(CASE WHEN tran_type = "Supplier Receive" then qty ELSE 0 END) as "supplier_receive",
            SUM(CASE WHEN tran_type = "Supplier Return" then qty ELSE 0 END) as "supplier_return"
            FROM `invoice_tran_items` where product_id = ' . $product_id . ' and created_at >= "' . $date . ' 00:00:00" and created_at <= "' . $date . ' 23:59:59"
        ');

        // set null value = 0 
        $stock_records_date[0]->sales = $stock_records_date[0]->sales == null ? 0 : $stock_records_date[0]->sales;
        $stock_records_date[0]->customer_return = $stock_records_date[0]->customer_return == null ? 0 : $stock_records_date[0]->customer_return;
        $stock_records_date[0]->supplier_receive = $stock_records_date[0]->supplier_receive == null ? 0 : $stock_records_date[0]->supplier_receive;
        $stock_records_date[0]->supplier_return = $stock_records_date[0]->supplier_return == null ? 0 : $stock_records_date[0]->supplier_return;


        $stockOpeningEndingRecords->sales_amount = $stock_records_date[0]->sales;
        $stockOpeningEndingRecords->sales_return_amount = $stock_records_date[0]->customer_return;
        $stockOpeningEndingRecords->supplier_receive_amount = $stock_records_date[0]->supplier_receive;
        $stockOpeningEndingRecords->supplier_return_amount = $stock_records_date[0]->supplier_return;
        $stockOpeningEndingRecords->ending_stock = $stockOpeningEndingRecords->opening_amount -  $stock_records_date[0]->sales + $stock_records_date[0]->customer_return + $stock_records_date[0]->supplier_receive - $stock_records_date[0]->supplier_return;

        $stockOpeningEndingRecords->save();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
}
