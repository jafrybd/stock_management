<?php

namespace App\Http\Controllers;

use App\Models\GeneralExpense;
use App\Models\GeneralExpenseMonth;
use App\Models\RegularExpense;
use App\Http\Controllers\CommonController;
use App\Models\Journal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\DB;

class TotalExpenseController extends Controller
{

    private $common_class_obj = "";

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $toMonthFirstDay = date('Y-m-d');
        $nextMonthFirstDate =  date('Y-m-d');
        $search_month = 0;
        $search_year = 2021;


        if ($request->datepicker == null) {
            $todaysMonthYear = date("F, Y");
        } else {
            $todaysMonthYear = $request->datepicker;
        }

        try {
            $tempTodaysMonthYear = join("", explode(" ", $todaysMonthYear)); // Remove space  ->  "December, 2020" to "December,2020"
            $tempTodaysMonthYear = explode(",", $tempTodaysMonthYear);

            $month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            $temp_month = array_search($tempTodaysMonthYear[0], $month, true) + 1; // 12

            // get Search Month and year
            $search_month = $temp_month - 1;
            $search_year = $tempTodaysMonthYear[1];

            // Generate  toMonthFirstDay
            $temp_month = $temp_month < 10 ? ('0' . $temp_month) : $temp_month;
            $toMonthFirstDay = $tempTodaysMonthYear[1] . "-" . $temp_month . "-" . "01"; // 2020-12-01



            // Generate  nextMonthFirstDate
            $temp_month = $temp_month + 1;
            if ($temp_month > 12) {
                $temp_month = 1;
                $tempTodaysMonthYear[1] = (int) $tempTodaysMonthYear[1] + 1;
            }



            $temp_month = $temp_month < 10 ? ('0' . $temp_month) : $temp_month;
            $nextMonthFirstDate = $tempTodaysMonthYear[1] . "-" . $temp_month . "-" . "01";  // 2021-01-01



        } catch (\Throwable $th) {
            //throw $th;
        }


        $regularExpense = RegularExpense::where([['status', '=', 1], ['store_id', '=', 1], ['year', '=', $search_year], ['month', '=', $search_month]])->get();
        $generalExpenseMonthsId = GeneralExpenseMonth::where('month', '=', $search_month)->select('general_expense_id');

        $generalExpense = GeneralExpense::whereIn('id', $generalExpenseMonthsId)
            ->where('status', 1)
            ->get();

        //  This code is use for update or create journal for General Expense  
        $this->common_class_obj->generateJournalForGeneralExpense();






        $list = array();
        $totalAmount = 0;

        for ($i = 0; $i < count($regularExpense); $i++) {
            $totalAmount += $regularExpense[$i]->amount;
            array_push($list, array(
                "id" => $regularExpense[$i]->id,
                "title" => $regularExpense[$i]->title,
                "amount" => $regularExpense[$i]->amount,
                "expense_type" => "regular"
            ));
        }

        for ($i = 0; $i < count($generalExpense); $i++) {
            $totalAmount += $generalExpense[$i]->amount;

            array_push($list, array(
                "id" => $generalExpense[$i]->id,
                "title" => $generalExpense[$i]->title,
                "amount" => $generalExpense[$i]->amount,
                "expense_type" => "general"
            ));
        }
        // dump($regularExpense);
        // dump($generalExpense);
        // dd($list);


        $months = $this->common_class_obj->getMonthsName();
        return view('total-expense.index', compact('list', 'todaysMonthYear', 'totalAmount'));
    }
}
