<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \PDF;
use Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $adminList = User::where([['store_id', '=', 1], ['status', '=', 1],['user_type', '=','super']])->get();
        
        
        return view('admin.list',compact('adminList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validatedata = $request->validate([
            'name' =>'required | string | max:255',
            'email' => 'required|string| email| max:255'
        ]);

        $defaultPassword = "12345678";

        if (User::where([['email', '=', $request->email],['status','=',1]])->first()) {
            return redirect()->route('admins.index')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }

        $admin = new User();
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->store_id =  1;
        $admin->user_type =  'super';
        $admin->password = Hash::make($defaultPassword);

        

        $admin->save();

        if ($admin) {
            return redirect()->route('admins.index')->with('success', 'Admin ' . $request->input('name') . ' Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //

        
        
    }

    public function updateAdmin(Request $request)
    {
       
        $validatedata = $request->validate([
            'name' =>'required | string | max:255',
            'email' => 'required|string| email| max:255'
        ]);
        
        if (User::where([['email', '=', $request->email],['id','!=', $request->id],['status','=',1]])->first()) {
            return redirect()->route('admins.index')->with('error', 'This Email  ' . $request->input('email') . ' Already Used');
         }

        $admin = User::findorFail($request->id);
        
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->save();

        if ($admin) {
            return redirect()->route('admins.index')->with('success', 'Admin ' . $request->input('name') . ' Upadated Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $admin = User::findorFail($id);
        
        $admin->status = 0;

        $admin->save();

        return redirect()->route('admins.index')->with('success', 'Admin ' . $admin->name . ' Deleted Successfully');
    }
}
