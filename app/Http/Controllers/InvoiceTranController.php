<?php

namespace App\Http\Controllers;

use App\Models\InvoiceTran;
use App\Models\InvoiceTranItem;
use App\Http\Controllers\CommonController;
use App\Models\Stocks;
use App\Models\Supplier;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use DB;
use \PDF;
use Carbon\Carbon;
use Facade\FlareClient\Http\Response;
use Illuminate\Support\Facades\DB as FacadesDB;

class InvoiceTranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice_tran_list = InvoiceTran::where([['status', 1]])->orderBy('id', 'DESC')->get();
        // $d = $invoice_tran_list[0]->created_at;
        // dd((getDate(($d))));
        return view('invoice.index', compact('invoice_tran_list'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function advanceSales()
    {
        $invoice_tran_list = InvoiceTran::where([['status', 2], ['is_advance_sales', 1]])->orderBy('id', 'DESC')->get();
        return view('invoice.advanceSalesIndex', compact('invoice_tran_list'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function complete_advance_sales(Request $request)
    {
        $validatedata = $request->validate([
            'id' => 'required|numeric',
        ]);

        $id = $request->id;

        $invoice_tran_list = InvoiceTran::where([['status', 2], ["id", $id], ['is_advance_sales', 1]])->orderBy('id', 'DESC')->get();

        if (count($invoice_tran_list) < 1) {
            dd("Unknown 404 Error");
        }

        $invoice_tran_item_list = InvoiceTranItem::where([['status', 2], ['invoice_tran_id', '=', $id]])->orderBy('id', 'ASC')->get();

        for ($i = 0; $i < count($invoice_tran_item_list); $i++) {

            if ($invoice_tran_item_list[$i]->is_advance_sales == 1 && $invoice_tran_item_list[$i]->status == 2) {

                $temp_qty = 0;


                if (strtoupper($invoice_tran_item_list[$i]->quantity_as) == 'TON') {
                    $temp_qty = $invoice_tran_item_list[$i]->qty * 907.185;
                } else if (strtoupper($invoice_tran_item_list[$i]->quantity_as) == 'BOX') {
                    $temp_qty = $invoice_tran_item_list[$i]->qty * $invoice_tran_item_list[$i]->product->quatity_value;
                } else if (strtoupper($invoice_tran_item_list[$i]->quantity_as) == 'KG') {
                    $temp_qty = $invoice_tran_item_list[$i]->qty;
                } else if (strtoupper($invoice_tran_item_list[$i]->quantity_as) == 'PCS') {
                    $temp_qty = $invoice_tran_item_list[$i]->qty;
                }


                // Get Stock info
                $stock_details = Stocks::where([['status', '=', 1], ['product_id', $invoice_tran_item_list[$i]->product_id]])->get();

                if (count($stock_details) > 0) {
                    $stock_details = $stock_details[0];
                } else {
                    return redirect()->route('invoice.details', [$id])->with('error', "Stock Not Found");
                }

                if ($temp_qty  > $stock_details->current_stock) {
                    return redirect()->route('invoice.details', [$id])->with('error', "Stock Not Available");
                }

                // Update Stock
                $stock_details->current_stock = $stock_details->current_stock -  $temp_qty;
                $stock_details->save();

                // update invoice item 
                $invoice_tran_item_list[$i]->status = 1;
                $invoice_tran_item_list[$i]->save();
            }
        }

        $invoice_tran_list[0]->status = 1;
        $invoice_tran_list[0]->is_advance_sales = 1;
        $invoice_tran_list[0]->save();

        return redirect()->route('invoice.details', [$id])->with('success', "Complete");

        // return view('invoice.advanceSalesIndex', compact('invoice_tran_list'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceTran  $InvoiceTran
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice_tran_list = InvoiceTran::where([['status', '!=', 0], ['id', '=', $id]])->orderBy('id', 'DESC')->get();

        if (count($invoice_tran_list) < 1) {
            dd("404 Unknown Request");
        }

        $invoice_tran_item_list = InvoiceTranItem::where([['status', '!=', 0], ['invoice_tran_id', '=', $id]])->orderBy('id', 'ASC')->get();

        // dump($invoice_tran_list[0]);
        // dd($invoice_tran_item_list[0]->product->product_heads->title);
        // dd($invoice_tran_item_list);

        return view('invoice.details', compact('invoice_tran_list', 'invoice_tran_item_list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InvoiceTran  $InvoiceTran
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceTran $InvoiceTran)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InvoiceTran  $InvoiceTran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceTran $InvoiceTran)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InvoiceTran  $InvoiceTran
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceTran $InvoiceTran)
    {
        //
    }

    public function invoiceSearch(Request $request)
    {
    

        $filter_transaction_type = $request->filter_transaction_type;
        $filter_date_range = explode('-',str_replace(' ', '', $request->filter_date_range));

        if(empty($filter_transaction_type) && empty($filter_date_range)){
            return new JsonResponse(json_encode(array("success" => false, "message" => "You must select any of the filter to search")));
        }

        $startDate = explode('/',$filter_date_range[0]);
        $endDate = explode('/',$filter_date_range[1]);

        $start = date($startDate[2].'-'.$startDate[0].'-'.$startDate[1]);
        $end = date($endDate[2].'-'.$endDate[0].'-'.$endDate[1]);

      
        
        $query = FacadesDB::table('invoice_trans');
        $query->where('status', '=',  1);
        
        if(!empty($filter_transaction_type)){
            $query->where('tran_type', '=',  $filter_transaction_type);
        }

        
            $query->whereBetween(DB::raw('DATE(created_at)'), array($start, $end));
        
        try {
            $invoices = $query->orderBy('id', 'ASC')->get();
        } catch (\Throwable $th) {
            $json = json_encode(array("success" => true, "message" => $th,));
            return new JsonResponse($json);
        }


         json_encode($invoices);
        //return new JsonResponse(json_encode(array("success" => true, "message" => "You must select a Year","data"=>$customer)));
         
         return view('invoice.invoice_table', compact('invoices'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InvoiceTran  $InvoiceTran
     * @return \Illuminate\Http\Response
     */
    public function generatePdf($id)
    {
        $invoice_tran_list = InvoiceTran::where([['status', '!=', 0], ['id', '=', $id]])->orderBy('id', 'DESC')->get();

        if (count($invoice_tran_list) < 1) {
            dd("404 Unknown Request");
        }

        $invoice_details = $this->common_class_obj->generateInvoicePDF($id);
        
        return $invoice_details;
        
        

        
    }
}
