<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\ProductType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $product_types = ProductType::where('status', 1)->get();
        
        return view('product_type.index', compact('product_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        if (ProductType::where([['title', '=', $request->input('title')],['status','=',1]])->first()) {
            return redirect()->route('product-type.index')->with('error', 'Product Type Already Exists');
         }

        $product_type = new ProductType();
        $product_type->title = $request->title;                    
        $product_type->created_by = Auth::user()->id;                   
        $product_type->save();

        if ($product_type) {
            return redirect()->route('product-type.index')->with('success', 'Product Type Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function show(ProductType $productType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $product_type = ProductType::findorFail($id);
        
        return view('product_type.edit', compact('product_type', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        $product_type = ProductType::findorFail($id);
        if($product_type->title != $request->title){


             $existingData = ProductType::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first();

            
            if (!empty($existingData) && $existingData->id  != $product_type->id) {
                return redirect()->route('product-type.index')->with('error', 'Product Type Already Exists');
            }


            $product_type->title = $request->title;
            $product_type->save();
           
            if ($product_type) {
                return redirect()->route('product-type.index')->with('success', 'Product Type Updated');
            }
        }
        else{
            return redirect()->route('product-type.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductType  $productType
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        //
        $product_type = ProductType::findorFail($id);

        try {
            $exit_product_list = Products::where([['status', '=', 1], ['product_type_id', $id]])->get();
            if (count($exit_product_list) > 0) {
                return redirect()->route('product-type.index')->with('error', 'You can\'t Delete this Product Type, It has Product.');
            }
        } catch (\Throwable $th) {
            return redirect()->route('product-type.index')->with('error', 'You can\'t Delete this Product Type, It has Product.');
        }
        
        $product_type->status =0;
        

        $product_type->save();
        return redirect()->route('product-type.index')->with('success', 'Product Type Delete');
    }
}
