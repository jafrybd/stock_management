<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\Supplier;
use App\Models\SupplierBalanceHistory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SupplierController extends Controller
{
    private $common_class_obj;
    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $supplier_list = Supplier::where('status', 1)->get();
        return view('supplier.index', compact('supplier_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11'
        ]);

        if (Supplier::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first()) {
            return redirect()->route('supplier.index')->with('error', 'Supplier Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $supplier = new Supplier();
        $supplier->name = $request->name;
        $supplier->contact_no = $request->phone;
        $supplier->company_name =  empty($request->company) ? "" : $request->company;
        $supplier->address =  empty($request->address) ? "" : $request->address;
        $supplier->created_by = Auth::user()->id;
        $supplier->updated_by = Auth::user()->id;

        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/suppliers'), $imageName);
            $supplier->image = $imageName;
        }

        $supplier->save();

        if ($supplier) {
            return redirect()->route('supplier.index')->with('success', 'Supplier ' . $request->input('name') . ' (' . $request->input('phone') . ') Added');
        }
    }

    /**
     * Display the specified resource.
     * @return \Illuminate\Http\Response
     */
    public function suppliers_date_wise_purchase(Request $request)
    {


        if ($request->search_date == null) {
            $today = date("Y-m-d");
        } else {
            $today = $request->search_date;
        }

        $list = array();
        $next_date = date('Y-m-d', strtotime($today . ' +1 day'));

        $purchase_list = DB::table('invoice_trans')
            ->select('supplier_id', DB::raw('SUM(total) as total'))
            ->where('tran_type', '=', 'Supplier Receive')
            ->where('created_at', '>=', $today)
            ->where('created_at', '<=', $next_date)
            ->groupBy('supplier_id')
            ->get();


        for ($i = 0; $i < count($purchase_list); $i++) {
            $supplier_details = Supplier::where('id', $purchase_list[$i]->supplier_id)->get();

            if (count($supplier_details) > 0) {
                $supplier_details = $supplier_details[0];
            } else {
                $supplier_details = array();
            }

            array_push($list, array(
                'supplier_id' =>  $purchase_list[$i]->supplier_id,
                'amount' => $purchase_list[$i]->total,
                'supplier_details' =>   $supplier_details,
            ));
        }

        return view('reports.suppliers_date_wise_purchase', compact('list', 'today'));
    }



    /**
     * Display the specified resource.
     * @return \Illuminate\Http\Response
     */

    public function product_wise_sales_reports_month(Request $request)
    {

        $toMonthFirstDay = date('Y-m-d');
        $nextMonthFirstDate =  date('Y-m-d');
        

        if ($request->datepicker == null) {
            $todaysMonthYear = date("F, Y");
        } else {
            $todaysMonthYear = $request->datepicker;
        }

     

        try {
           $tempTodaysMonthYear = join("",explode(" ",$todaysMonthYear)); // Remove space  ->  "December, 2020" to "December,2020"
           $tempTodaysMonthYear = explode(",",$tempTodaysMonthYear);

           $month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December' ]; 
           $temp_month = array_search($tempTodaysMonthYear[0],$month,true) + 1; // 12

           // Generate  toMonthFirstDay
           $temp_month = $temp_month < 10 ? ('0'.$temp_month) : $temp_month;
           $toMonthFirstDay = $tempTodaysMonthYear[1]. "-". $temp_month ."-". "01"; // 2020-12-01


           // Generate  nextMonthFirstDate
           $temp_month = $temp_month +1 ;
           if($temp_month > 12){
               $temp_month = 1;
               $tempTodaysMonthYear[1] = (int) $tempTodaysMonthYear[1] + 1;
           }

           $temp_month = $temp_month < 10 ? ('0'.$temp_month) : $temp_month;
           $nextMonthFirstDate = $tempTodaysMonthYear[1]. "-". $temp_month ."-". "01";  // 2021-01-01
          
        } catch (\Throwable $th) {
            //throw $th;
        }


        $list = array();

        $product_list = DB::table('invoice_tran_items')
            ->select('product_id', 'quantity_as', DB::raw('SUM(qty) as qty_total'))
            ->where('tran_type', '=', 'sales to customer')
            ->where('created_at', '>=', $toMonthFirstDay)
            ->where('created_at', '<=', $nextMonthFirstDate)
            ->groupBy('product_id')
            ->get();


        for ($i = 0; $i < count($product_list); $i++) {
            $product_details = Products::where('id', $product_list[$i]->product_id)->get();

            if (count($product_details) > 0) {
                $product_details = $product_details[0];
            } else {
                $product_details = array();
            }

            array_push($list, array(
                'product_id' =>  $product_list[$i]->product_id,
                'qty_total' => $product_list[$i]->qty_total,
                'quantity_as' => $product_list[$i]->quantity_as,
                'product_details' =>   $product_details,
            ));

           
        }

        // dd($list[0]['product_details']->product_heads->title);
        // dd($list);
        
        return view('reports.product_wise_sales_reports_month', compact('list', 'todaysMonthYear'));

    }

 


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier_info = Supplier::findorFail($id);
        return view('supplier.edit', compact('supplier_info', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedata = $request->validate([
            'name' => 'required|max:30|min:5',
            'phone' => 'required|max:11|min:11'
        ]);

        $custom_info = Supplier::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('supplier.index')->with('error', 'Supplier Unknown');
        }

        $exitSupplierDetails = Supplier::where([['contact_no', '=', $request->input('phone')], ['status', '=', 1]])->first();

        if (!empty($exitSupplierDetails) && $exitSupplierDetails->id  != $custom_info->id) {
            return redirect()->route('supplier.index')->with('error', 'Supplier Phone No  ' . $request->input('phone') . ' Already Used');
        }

        $custom_info->name = $request->name;
        $custom_info->contact_no = $request->phone;
        $custom_info->company_name =  empty($request->company) ? "" : $request->company;
        $custom_info->address =  empty($request->address) ? "" : $request->address;
        $custom_info->created_by = Auth::user()->id;
        $custom_info->updated_by = Auth::user()->id;


        if ($request->hasFile('photo')) { // if image is not empty.
            $request->validate([
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            $imageName = time() . '.' . $request->photo->extension();
            $request->photo->move(public_path('images/suppliers'), $imageName);
            $custom_info->image = $imageName;
        }


        $custom_info->save();

        if ($custom_info) {
            return redirect()->route('supplier.index')->with('success', 'Supplier info Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $custom_info = Supplier::findorFail($id);
        $custom_info->status = 0;
        $custom_info->save();

        return redirect()->route('supplier.index')->with('success', 'Supplier ' . $custom_info->name . ' ( ' . $custom_info->contact_no . ' ) Deleted');
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function getSupplierInfoByPhoneNo(Request $request)
    {
        $phone = $request->phoneNo;
        $customs_list =  Supplier::where([['status', 1], ['contact_no', '=', $phone]])->get();

        $response = array("success" => true, "isFindData" => false, "data" => "{}", "message" => "");

        if (count($customs_list) > 0) {
            $response['isFindData'] =  true;
            $response['data'] = $customs_list[0];
        }

        return new JsonResponse(json_encode($response));
    }


    public function payment(Request $request)
    {
        $validatedata = $request->validate([
            'amount' => 'required|min:1|numeric'

        ]);

        $id = $request->id;
        $supplier_name = $request->supplier_name;
        $amount = $request->amount;
        $transaction_type = $request->transaction_type;

        $supplier_info = Supplier::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('supplier.index')->with('error', 'Supplier Unknown');
        }

        $last_payable = $supplier_info->payable;
        $last_receivable = $supplier_info->receivable;

        // dd($transaction_type);

        /// Payable
        if ($transaction_type == 0) {
            if ($supplier_info->payable <= 0) {
                return redirect()->route('supplier.index')->with('error', 'Nothing for Payable');
            } else {
                if ($amount >= $supplier_info->payable) {
                    $new_amount = $amount - $supplier_info->payable;
                    if ($new_amount >= 0) {
                        $supplier_info->payable = 0;
                        $supplier_info->receivable += $new_amount;
                    }
                } else {
                    $supplier_info->payable -= $amount;
                }
            }
        }
        /// Receivable
        else {
            if ($supplier_info->receivable <= 0) {
                return redirect()->route('supplier.index')->with('error', 'Nothing for Receivable');
            } else {
                if ($amount >= $supplier_info->receivable) {
                    $new_amount = $amount - $supplier_info->receivable;
                    if ($new_amount >= 0) {
                        $supplier_info->receivable = 0;
                        $supplier_info->payable += $new_amount;
                    }
                } else {
                    $supplier_info->receivable -= $amount;
                }
            }
        }

        $supplier_info->save();

        $supplierHistory = new SupplierBalanceHistory();
        $supplierHistory->supplier_id = $supplier_info->id;
        $supplierHistory->last_payable = $last_payable;
        $supplierHistory->current_payable = $supplier_info->payable;
        $supplierHistory->last_receivable = $last_receivable;
        $supplierHistory->current_receivable =  $supplier_info->receivable;
        $supplierHistory->amount =  $amount;
        $supplierHistory->transaction_type =  $transaction_type;
        $supplierHistory->reason =  "Manual";
        $supplierHistory->last_update = Carbon::now()->toDateTimeString();
        $supplierHistory->updated_by = Auth::user()->id;
        $supplierHistory->save();

        if ($supplier_info) {

            $journals = array();
            if ($transaction_type == "0") {

                array_push($journals, array(
                    "name" => "Account Payable",
                    "type" => "Dr",
                    "tran_type" => "SupplierBalanceHistory",
                    "tran_id" => $supplierHistory->id,
                    "amount" => $amount
                ));

                array_push($journals, array(
                    "name" => "Cash",
                    "type" => "Cr",
                    "description" =>  "Pay To Supplier",
                    "tran_type" => "SupplierBalanceHistory",
                    "tran_id" => $supplierHistory->id,
                    "amount" =>  $amount
                ));
            } else {

                array_push($journals, array(
                    "name" => "Cash",
                    "type" => "Dr",
                    "description" =>  "Receive From Supplier",
                    "tran_type" => "SupplierBalanceHistory",
                    "tran_id" => $supplierHistory->id,
                    "amount" => $amount
                ));

                array_push($journals, array(
                    "name" => "Account Payable",
                    "type" => "Cr",
                    "tran_type" => "SupplierBalanceHistory",
                    "tran_id" => $supplierHistory->id,
                    "amount" =>  $amount
                ));
            }

            $this->common_class_obj->saveJournal($journals);

            return redirect()->route('supplier.index')->with('success', 'Supplier Payment has been updated');
        }
    }

    public function balanceHistory($id)
    {

        $supplier_balance_history = SupplierBalanceHistory::where('supplier_id', '=', $id)->orderBy('id', 'DESC')->get();

        if (count($supplier_balance_history) < 1) {
            return redirect()->route('supplier.index')->with('error', 'No Data Exists');
        }

        $supplier_name = $supplier_balance_history[0]->supplier->name;
        $supplier_payable_balance = $supplier_balance_history[0]->supplier->payable;
        $supplier_receivable_balance = $supplier_balance_history[0]->supplier->receivable;


        return view('supplier.supplierBalanceHistory', compact('supplier_balance_history', 'id', 'supplier_name', 'supplier_payable_balance', 'supplier_receivable_balance'));
    }
}
