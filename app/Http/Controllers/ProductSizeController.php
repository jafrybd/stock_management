<?php

namespace App\Http\Controllers;

use App\Models\Products;
use App\Models\ProductSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductSizeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_sizes = ProductSize::where('status', 1)->get();
        return view('product_size.index', compact('product_sizes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        if (ProductSize::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first()) {
            return redirect()->route('product-size.index')->with('error', 'Product Size Already Exists');
        }

        $product_size = new ProductSize();
        $product_size->title = $request->title;
        $product_size->created_by = Auth::user()->id;
        $product_size->save();

        if ($product_size) {
            return redirect()->route('product-size.index')->with('success', 'Product Size Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductSize  $product_size
     * @return \Illuminate\Http\Response
     */
    public function show(ProductSize $product_size)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProductSize  $product_size
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product_size = ProductSize::findorFail($id);
        return view('product_size.edit', compact('product_size', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductSize  $product_size
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedata = $request->validate([
            'title' => 'required|max:255'
        ]);

        $product_size = ProductSize::findorFail($id);
        $req_id = $request->id;

        if ($req_id != $id) {
            return redirect()->route('product-size.index')->with('error', 'Product Size Unknown');
        }

        if ($product_size->title != $request->title) {
            $exitProductSizeDetails = ProductSize::where([['title', '=', $request->input('title')], ['status', '=', 1]])->first();

            
            if (!empty($exitProductSizeDetails) && $exitProductSizeDetails->id  != $product_size->id) {
                return redirect()->route('product-size.index')->with('error', 'Product  Size already Exists');
            }

            $product_size->title = $request->title;
            $product_size->save();

            if ($product_size) {
                return redirect()->route('product-size.index')->with('success', 'Product Size Updated');
            }
        } else {
            return redirect()->route('product-size.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductSize  $product_size
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product_size = ProductSize::findorFail($id);

        try {
            $exit_product_list = Products::where([['status', '=', 1], ['product_size_id', $id]])->get();
            if (count($exit_product_list) > 0) {
                return redirect()->route('product-size.index')->with('error', 'You can\'t Delete this Product Size, It has Product.');
            }
        } catch (\Throwable $th) {
            return redirect()->route('product-size.index')->with('error', 'You can\'t Delete this Product Size, It has Product.');
        }

        $product_size->status = 0;
        $product_size->save();

        return redirect()->route('product-size.index')->with('success', 'Product Size Delete');
    }
}
