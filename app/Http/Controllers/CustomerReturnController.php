<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\Customer;
use App\Models\CustomerBalanceHistory;
use App\Models\InvoiceTran;
use App\Models\InvoiceTranItem;
use App\Models\ProductCategory;
use App\Models\ProductGroup;
use App\Models\ProductHead;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductType;
use App\Models\Sales;
use App\Models\Stocks;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;


class CustomerReturnController extends Controller
{
    private $common_class_obj;

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product_list = Products::where('status', 1)->get();

        // $product_list[0]->product_head_id;
        // dump($product_list[0]['product_head_id']);



        for ($i = 0; $i < count($product_list); $i++) {


            $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_list[$i]->id]])->get();
            $product_head_list = ProductHead::where('status', '=', 1)->get();
            $product_size_list = ProductSize::where('status', '=', 1)->get();
            $product_cat_list = ProductCategory::where('status', '=', 1)->get();
            $product_type_list = ProductType::where('status', '=', 1)->get();
            $product_group_list = ProductGroup::where('status', '=', 1)->get();


            //dd($stock_details);

            if (!empty($stock_details)) {
                $product_list[$i]['stock'] = $stock_details[0]['current_stock'];
                $product_list[$i]['selling_price'] = $stock_details[0]['selling_price'];
                $product_list[$i]['buying_price'] = $stock_details[0]['buying_price'];
            } else {
                $product_list[$i]['stock'] = 0;
                $product_list[$i]['selling_price'] = 0;
                $product_list[$i]['buying_price'] = 0;
            }



            // Get product Head
            if ($product_list[$i]['product_head_id'] != 0) {
                $product_head_details = ProductHead::where([['id', '=', $product_list[$i]['product_head_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $product_list[$i]['product_head'] = $product_head_details;
                }
            }

            // Get product Category
            if ($product_list[$i]['product_category_id'] != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $product_list[$i]['product_category_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $product_list[$i]['product_category'] = $product_cat_details;
                }
            }

            // Get product type
            if ($product_list[$i]['product_type_id'] != 0) {
                $product_type_details = ProductType::where([['id', '=', $product_list[$i]['product_type_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $product_list[$i]['product_type'] = $product_type_details;
                }
            }


            // Get product Size
            if ($product_list[$i]['product_size_id'] != 0) {
                $product_size_details = ProductSize::where([['id', '=', $product_list[$i]['product_size_id']], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $product_list[$i]['product_size'] = $product_size_details;
                }
            }


            // Get product Group
            if ($product_list[$i]['product_group_id'] != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $product_list[$i]['product_group_id']], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $product_list[$i]['product_group'] = $product_group_details;
                }
            }
        }

        // dd($product_list);


        return view('customer_return.index', compact('product_list', 'product_head_list', 'product_size_list', 'product_cat_list', 'product_type_list',  'product_group_list'));
    }

    public function add_to_cart(Request $request)
    {
        $product_id = $request->product_id;
        $qty = $request->qty;
        $qty_as = $request->qty_as;
        $transaction_type = 'Customer Return';


        // Check Product id & qty
        try {
            $product_id = (int)$product_id;
            $qty = (float)$qty;
        } catch (\Throwable $th) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Request Data Formate Error")));
        }

        // Get product details & Stock Details
        $product_details = Products::where([['status', '=', 1], ['id', $request->product_id]])->get();
        $stock_details = Stocks::where([['status', '=', 1], ['product_id', $request->product_id]])->get();

        if (count($product_details) > 0) {
            $product_details = $product_details[0];
        } else {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product")));
        }



        // Check qty calculation method & calculation qty
        // 1 ton = 907.185 kg 
        $temp_qty = 0;

        if (strtoupper($product_details->quantity) == "KG/TON") {
            if (strtoupper($qty_as) == "KG") {
                $temp_qty = $qty;
            } else if (strtoupper($qty_as) == "TON") {
                $temp_qty = $qty * 907.185;
            } else {
                return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Qty Method", "qty_as" => $qty_as)));
            }
        } else {
            if (strtoupper($qty_as) == "BOX") {
                try {
                    $temp_qty =  $qty * $product_details->quatity_value;
                } catch (\Throwable $th) {
                    return new JsonResponse(json_encode(array("success" => false, "message" => "Try catch error", "qty_as" => $product_details)));
                }
            } else if (strtoupper($qty_as) == "PCS") {
                $temp_qty = $qty;
            } else {
                return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Qty Method", "qty_as" => $qty_as)));
            }
        }


        if (count($stock_details) > 0) {
            $stock_details = $stock_details[0];
        } else {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Unknown Product Stock")));
        }

        // Check this product is already added in Carts
        $carts_list = Carts::where([['created_by', Auth::user()->id], ['product_id', '=', $product_id], ['transaction_type', '=', $transaction_type]])->orderBy('id', 'ASC')->get();
        $qty_already_added = 0;

        // calculate qty whih already added
        for ($i = 0; $i < count($carts_list); $i++) {
            if (strtoupper($carts_list[$i]->quantity_as) == "BOX") {
                $qty_already_added = $carts_list[$i]->quantity * $product_details->quatity_value;
            } else if (strtoupper($carts_list[$i]->quantity_as) == 'TON') {
                $qty_already_added = $carts_list[$i]->quantity * 907.185;
            } else {
                $qty_already_added = $carts_list[$i]->quantity;
            }

            break;
        }


        if ($qty < 1) {
            return new JsonResponse(json_encode(array("success" => false, "message" => "Please give valid qty which is greater then zero")));
        }


        if (count($carts_list) > 0) {
            $cart = $carts_list[0];

            if (strtoupper($qty_as) != strtoupper($carts_list[0]->quantity_as)) {
                $cart->quantity = $temp_qty + $qty_already_added;

                if (strtoupper($qty_as) == "BOX" || strtoupper($qty_as) == "PCS") {
                    $cart->quantity_as = strtoupper("PCS");
                } else {
                    $cart->quantity_as = strtoupper("KG");
                }
            } else {
                $cart->quantity = $cart->quantity + $qty;
            }
        } else {
            $cart = new Carts();
            $cart->product_id = $product_id;
            $cart->quantity_as = strtoupper($qty_as);
            $cart->quantity = $qty;
            $cart->selling_price = $stock_details->selling_price;
            $cart->transaction_type = $transaction_type;
            $cart->created_by =  Auth::user()->id;
        }

        $cart->save();

        $json = json_encode(array("success" => true, "message" => "Product added to cart.", "carts_list" => $carts_list));
        return new JsonResponse($json);
    }

    public function selectedCarts()
    {

        $carts = Carts::where([['created_by', Auth::user()->id], ['transaction_type', '=', 'Customer Return']])->orderBy('id', 'ASC')->get();
        $subtotal = 0;
        for ($i = 0; $i < count($carts); $i++) {

            // quantity_as

            // dd($carts[$i]->product->quatity_value);

            $temp_qty = $carts[$i]->quantity;

            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
            } else {
                dd("404");
            }

            // dump($carts[$i]->selling_price);
            // dump($carts[$i]->selling_price * $temp_qty) ;
            // dd($temp_qty);

            $carts[$i]['total'] = $carts[$i]->selling_price * $temp_qty;
            $subtotal += $carts[$i]->selling_price * $temp_qty;

            // Get product Head
            if ($carts[$i]->product->product_head_id != 0) {
                $product_head_details = ProductHead::where([['id', '=', $carts[$i]->product->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $carts[$i]['product_head'] = $product_head_details;
                }
            }

            // // Get product Category
            if ($carts[$i]->product->product_category_id != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $carts[$i]->product->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $carts[$i]['product_category'] = $product_cat_details;
                }
            }

            // // Get product type
            if ($carts[$i]->product->product_type_id  != 0) {
                $product_type_details = ProductType::where([['id', '=', $carts[$i]->product->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $carts[$i]['product_type'] = $product_type_details;
                }
            }


            // // Get product Size
            if ($carts[$i]->product->product_size_id != 0) {
                $product_size_details = ProductSize::where([['id', '=', $carts[$i]->product->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $carts[$i]['product_size'] = $product_size_details;
                }
            }


            // // Get product Group
            if ($carts[$i]->product->product_group_id != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $carts[$i]->product->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $carts[$i]['product_group'] = $product_group_details;
                }
            }
        }

        // dd($carts);


        return view('customer_return.selectedSales', compact('carts', 'subtotal'));
    }


    function redirectFun($tran_type, $message = "", $isError = true)
    {
        if ($tran_type == "Customer Return") {
            return redirect()->route('customer_return.selectedCarts')->with($isError == true ? 'error' : 'success', $message);
        } else {
            return redirect()->route('home')->with($isError == true ? 'error' : 'success', 'Unknown Request');
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function remove_from_cart(Request $request)
    {
        function redirectFun($tran_type, $message = "")
        {
            if ($tran_type == "Customer Return") {
                return redirect()->route('customer_return.selectedCarts')->with('error', $message);
            } else {
                return redirect()->route('home')->with('error', 'Unknown Request');
            }
        }


        $carts = Carts::where([['created_by', Auth::user()->id], ['id', '=', $request->id]])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return redirectFun($request->tran_type, "Carts not found");
        }

        Carts::where('id', '=', $carts[0]->id)->delete();
        return redirectFun($request->tran_type, "Product Remove from Carts");
    }

    public function info_update(Request $request)
    {
        $id = $request->id != null ? $request->id : 0;
        $product_id = $request->product_id != null ? $request->product_id : 0;
        $qty_as = $request->qty_as;
        $qty = $request->qty != null ? $request->qty : 0;
        $price = $request->price != null ? $request->price : 0;
        $tempSubtotal = 0;

        if ($price < 1) {
            return $this->redirectFun($request->tran_type, "Please give valid Price which is greater then zero");
        }


        if (is_null($qty_as)) {
            return $this->redirectFun($request->tran_type, "Please select Qty_as");
        }

        try {
            $product_id = (int)$product_id;
            $id = (int)$id;
            $qty = (float)$qty;
        } catch (\Throwable $th) {
            return $this->redirectFun($request->tran_type, "Request Data Format Error");
        }

        $carts = Carts::where([['created_by', Auth::user()->id], ['id', '=', $id], ['product_id', '=', $product_id]])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return $this->redirectFun($request->tran_type, "Carts not found");
        }

        $product_details = Products::where([['status', '=', 1], ['id', $product_id]])->get();
        $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_id]])->get();

        if (count($product_details) > 0) {
            $product_details = $product_details[0];
        } else {
            return $this->redirectFun($request->tran_type, "Unknown Product");
        }

        if (count($stock_details) > 0) {
            $stock_details = $stock_details[0];
        } else {
            return $this->redirectFun($request->tran_type, "Unknown Product Stock");
        }

        // calculate qty whih already added
        $temp_qty = 0;

        if (strtoupper($qty_as) == 'TON') {
            $temp_qty = $qty * 907.185;
        } else if (strtoupper($qty_as) == 'BOX') {
            $temp_qty = $qty * $product_details->quatity_value;
        } else if (strtoupper($qty_as) == 'KG') {
            $temp_qty = $qty;
        } else if (strtoupper($qty_as) == 'PCS') {
            $temp_qty = $qty;
        } else {
            return $this->redirectFun($request->tran_type, "Please select Qty_as 000");
        }


        if ($temp_qty < 1) {
            return $this->redirectFun($request->tran_type, "Please give valid qty which is greater then zero");
        }

        $carts[0]->quantity_as = strtoupper($qty_as);
        $carts[0]->quantity = $qty;
        $carts[0]->selling_price = $price;
        $carts[0]->created_by =  Auth::user()->id;
        $carts[0]->save();


        return $this->redirectFun($request->tran_type, "Cart Update", false);
    }

    public function sales_checkout(Request $request)
    {
        $sub_total = $request->sub_total != null ? $request->sub_total : 0;
        $total = $request->total != null ? $request->total : 0;
        $discount = $request->discount != null ? $request->discount : 0;
        $phone_no = $request->phone_no != null ? $request->phone_no : "0";
        $paid = $request->paid != null ? $request->paid : 0;
        $tempSubtotal = 0;


        // Phone number check must be 11 digit, startwith 01, all number is digit
        try {

            if (strlen($phone_no) != 11) {
                return redirect()->route('customer_return.selectedCarts')->with('error', 'Phone Number Must be 11 digit');
            } else if ($phone_no[0] != "0" || $phone_no[1] != "1") {
                return redirect()->route('customer_return.selectedCarts')->with('error', 'Give a valid Phone Number like (01...)');
            }

            $a = (int) $phone_no; // Chech phone_no in number
            if (strlen(strval($a)) != 10) {
                return redirect()->route('customer_return.selectedCarts')->with('error', 'Give a valid Phone Number like (01...)');
            }
        } catch (\Throwable $th) {
            return redirect()->route('customer_return.selectedCarts')->with('error', 'Give a valid Phone Number like (01...)');
        }


        $carts = Carts::where([['created_by', Auth::user()->id], ['transaction_type', '=', 'Customer Return']])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return redirect()->route('customer_return.selectedCarts')->with('error', 'Cart is empty');
        }

        $temp_qty = 0;

        for ($i = 0; $i < count($carts); $i++) {
            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
                $quantity_as = $carts[$i]->quantity_as;
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
                $quantity_as = $carts[$i]->quantity_as;
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
                $quantity_as = $carts[$i]->quantity_as;
            }
            $tempSubtotal += $carts[$i]->selling_price * $temp_qty;
        }

        if ($tempSubtotal != $sub_total) {
            return redirect()->route('customer_return.selectedCarts')->with('error', 'Something Wrong');
        }


        // update customer Account
        // if customer phone number exit then user it or create new account
        $custome_name = $request->custome_name;
        $customs_list =  Customer::where([['status', 1], ['contact_no', '=', $phone_no]])->get();
        $last_payable = 0;
        $last_receivable = 0;


        $due = $total - $paid;

        if (count($customs_list) == 0) {
            $customer = new Customer();
            $customer->name = strlen($custome_name) == 0 ? "Unknown Name" : $custome_name;
            $customer->contact_no = $phone_no;
            $customer->description =  "";
            $customer->address =  "";
            $customer->payable = $due > 0 ? $due : 0;
            $customer->created_by = Auth::user()->id;
            $customer->updated_by = Auth::user()->id;
            $customer->save();
        } else {

            $last_payable = $customs_list[0]->payable;
            $last_receivable = $customs_list[0]->receivable;


            // Calculate payable and receivable
            $customer = $customs_list[0];
            //     if ($due > 0) {
            //         if ($customer->receivable > 0) {
            //             if ($customer->receivable >= $due) {
            //                 $customer->receivable -= $due;
            //             } else {
            //                 $due -=  $customer->receivable;
            //                 $customer->receivable = 0;
            //                 $customer->payable += $due;
            //             }
            //         } else {
            //             $customer->payable += $due;
            //         }
            //     }
            //     $customer->save();
            // }
            if ($due > 0) {
                if ($customer->receivable > 0) {
                    if ($customer->receivable >= $due) {
                        $customer->receivable -= $due;
                    } else {
                        $due -=  $customer->receivable;
                        $customer->receivable = 0;
                        $customer->payable += $due;
                    }
                } else {
                    $customer->payable += $due;
                }
            }

            $customer->save();
        }



        /// Customer Balance History
        if ($due > 0) {
            $customerHistory = new CustomerBalanceHistory();
            $customerHistory->customer_id = $customer->id;
            $customerHistory->last_payable = $last_payable;
            $customerHistory->current_payable = $customer->payable == null ? 0 : $customer->payable;
            $customerHistory->last_receivable = $last_receivable;
            $customerHistory->current_receivable =  $customer->receivable == null ? 0 : $customer->receivable;
            $customerHistory->amount =  $due;
            $customerHistory->transaction_type =  0; // Payable
            $customerHistory->reason =  "Customer Return";
            $customerHistory->last_update = Carbon::now()->toDateTimeString();
            $customerHistory->updated_by = Auth::user()->id;
            $customerHistory->save();
        }


        //  Create Invoice Tran
        $InvoiceTran = new InvoiceTran();
        $InvoiceTran->customer_id = $customer->id;
        $InvoiceTran->inv_no = "inv2021";
        $InvoiceTran->tran_type = "Customer Return";
        $InvoiceTran->sub_total = $sub_total;
        $InvoiceTran->paid = $paid;
        $InvoiceTran->due = $total - $paid;
        $InvoiceTran->discount = $discount;
        $InvoiceTran->total = $total;
        $InvoiceTran->created_by = Auth::user()->id;
        $InvoiceTran->updated_by = Auth::user()->id;

        $InvoiceTran->save();
        $invoice_tran_id = $InvoiceTran->id;
        $transaction_type = $InvoiceTran->tran_type;

        //  Create Invoice Tran item

        for ($i = 0; $i < count($carts); $i++) {

            // Qty calculation
            $temp_qty = 0;
            $temp_quantity_as = '';

            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
                $temp_quantity_as = 'TON';
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
                $temp_quantity_as = 'BOX';
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
                $temp_quantity_as = 'KG';
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
                $temp_quantity_as = 'PCS';
            }

            $tempSubtotal += $carts[$i]->selling_price *  $temp_qty;

            $InvoiceTranItem = new InvoiceTranItem();
            $InvoiceTranItem->invoice_tran_id =  $invoice_tran_id;
            $InvoiceTranItem->product_id = $carts[$i]->product_id;
            $InvoiceTranItem->tran_type = "Customer Return";
            $InvoiceTranItem->qty =  $temp_qty;
            $InvoiceTranItem->quantity_as =  $temp_quantity_as;
            $InvoiceTranItem->price_per_rate = $carts[$i]->selling_price;
            $InvoiceTranItem->discount = 0;
            $InvoiceTranItem->created_by = Auth::user()->id;
            $InvoiceTranItem->updated_by = Auth::user()->id;

            $InvoiceTranItem->save();

            //update Stock
            $stock_details = Stocks::where([['status', '=', 1], ['product_id', $carts[$i]->product_id]])->get();
            if (count($stock_details) > 0) {
                $stock_details[0]->current_stock = $stock_details[0]->current_stock +  $temp_qty;
                $stock_details[0]->save();
            }

            // Delete from carts
            Carts::where('id', '=', $carts[$i]->id)->delete();
        }

        // Journal
        $this->common_class_obj->generateJournalFromInvoice($InvoiceTran);

        // ------------- Invoice
        $invoice_details = $this->common_class_obj->generateInvoicePDF($invoice_tran_id);

        return $invoice_details;

        //return redirect()->route('customer_return.selectedCarts')->with('success', 'Sales Complate');


    }
}
