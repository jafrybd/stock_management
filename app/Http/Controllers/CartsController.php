<?php

namespace App\Http\Controllers;

use App\Models\Carts;
use App\Models\Customer;
use App\Models\CustomerBalanceHistory;
use App\Models\InvoiceTran;
use App\Models\InvoiceTranItem;
use App\Models\ProductHead;
use App\Models\ProductGroup;
use App\Models\ProductCategory;
use App\Models\Products;
use App\Models\ProductSize;
use App\Models\ProductType;
use App\Models\Stocks;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\isNull;

class CartsController extends Controller
{
    private $common_class_obj = "";

    public function __construct()
    {
        $this->middleware('auth');
        $this->common_class_obj = new CommonController();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $carts = Carts::where([['created_by', Auth::user()->id], ['transaction_type', '=', 'Sales']])->orderBy('id', 'ASC')->get();
        $subtotal = 0;
        for ($i = 0; $i < count($carts); $i++) {

            // quantity_as

            // dd($carts[$i]->product->quatity_value);

            $temp_qty = $carts[$i]->quantity;

            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
            } else {
                dd("404");
            }

            // dump($carts[$i]->selling_price);
            // dump($carts[$i]->selling_price * $temp_qty) ;
            // dd($temp_qty);

            $carts[$i]['total'] = $carts[$i]->selling_price * $temp_qty;
            $subtotal += $carts[$i]->selling_price * $temp_qty;

            // Get product Head
            if ($carts[$i]->product->product_head_id != 0) {
                $product_head_details = ProductHead::where([['id', '=', $carts[$i]->product->product_head_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_head_details)) {
                    $carts[$i]['product_head'] = $product_head_details;
                }
            }

            // // Get product Category
            if ($carts[$i]->product->product_category_id != 0) {
                $product_cat_details = ProductCategory::where([['id', '=', $carts[$i]->product->product_category_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_cat_details)) {
                    $carts[$i]['product_category'] = $product_cat_details;
                }
            }

            // // Get product type
            if ($carts[$i]->product->product_type_id  != 0) {
                $product_type_details = ProductType::where([['id', '=', $carts[$i]->product->product_type_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_type_details)) {
                    $carts[$i]['product_type'] = $product_type_details;
                }
            }


            // // Get product Size
            if ($carts[$i]->product->product_size_id != 0) {
                $product_size_details = ProductSize::where([['id', '=', $carts[$i]->product->product_size_id], ['status', '=', 1]])->select('title', 'status')->first();
                if (!empty($product_size_details)) {
                    $carts[$i]['product_size'] = $product_size_details;
                }
            }


            // // Get product Group
            if ($carts[$i]->product->product_group_id != 0) {
                $product_group_details = ProductGroup::where([['id', '=', $carts[$i]->product->product_group_id], ['status', '=', 1]])->select('group_name', 'status')->first();
                if (!empty($product_group_details)) {
                    $carts[$i]['product_group'] = $product_group_details;
                }
            }
        }

        // dd($carts);


        return view('carts.selectedSales', compact('carts', 'subtotal'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function info_update(Request $request)
    {

        $id = $request->id != null ? $request->id : 0;
        $product_id = $request->product_id != null ? $request->product_id : 0;
        $qty_as = $request->qty_as;
        $qty = $request->qty != null ? $request->qty : 0;
        $price = $request->price != null ? $request->price : 0;
        $sales_type = $request->sales_type != null ? $request->sales_type : 0;
        $tempSubtotal = 0;

        if ($price < 1) {
            return $this->redirectFun($request->tran_type, "Please give valid Price which is getter then zero");
        }


        if (is_null($qty_as)) {
            return $this->redirectFun($request->tran_type, "Please select Qty_as");
        }

        try {
            $product_id = (int)$product_id;
            $id = (int)$id;
            $qty = (float)$qty;
        } catch (\Throwable $th) {
            return $this->redirectFun($request->tran_type, "Request Data Formate Error");
        }

        $carts = Carts::where([['created_by', Auth::user()->id], ['id', '=', $id], ['product_id', '=', $product_id]])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return $this->redirectFun($request->tran_type, "Carts not found");
        }

        $product_details = Products::where([['status', '=', 1], ['id', $product_id]])->get();
        $stock_details = Stocks::where([['status', '=', 1], ['product_id', $product_id]])->get();

        if (count($product_details) > 0) {
            $product_details = $product_details[0];
        } else {
            return $this->redirectFun($request->tran_type, "Unknown Product");
        }

        if (count($stock_details) > 0) {
            $stock_details = $stock_details[0];
        } else {
            return $this->redirectFun($request->tran_type, "Unknown Product Stock");
        }

        // calculate qty whih already added
        $temp_qty = 0;

        if (strtoupper($qty_as) == 'TON') {
            $temp_qty = $qty * 907.185;
        } else if (strtoupper($qty_as) == 'BOX') {
            $temp_qty = $qty * $product_details->quatity_value;
        } else if (strtoupper($qty_as) == 'KG') {
            $temp_qty = $qty;
        } else if (strtoupper($qty_as) == 'PCS') {
            $temp_qty = $qty;
        } else {
            return $this->redirectFun($request->tran_type, "Please select Qty_as 000");
        }


        if ($temp_qty > $stock_details->current_stock) {
            if ($sales_type != 1) { // if it's 1 then we allow advance sales
                return $this->redirectFun($request->tran_type, "Stock not available");
            }
        } else if ($temp_qty < 1) {
            return $this->redirectFun($request->tran_type, "Please give valid qty which is getter then zero");
        }

        $carts[0]->quantity_as = strtoupper($qty_as);
        $carts[0]->quantity = $qty;
        $carts[0]->selling_price = $price;
        $carts[0]->created_by =  Auth::user()->id;
        $carts[0]->is_allow_advance_sale =  $sales_type == 1 ? 1 : 0;
        $carts[0]->save();


        return $this->redirectFun($request->tran_type, "Cart Update", false);
    }

    function redirectFun($tran_type, $message = "", $isError = true)
    {
        if ($tran_type == "sales") {
            return redirect()->route('carts.index')->with($isError == true ? 'error' : 'success', $message);
        } else {
            return redirect()->route('home')->with($isError == true ? 'error' : 'success', 'Unknown Request');
        }
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function remover_from_carts(Request $request)
    {
        function redirectFun($tran_type, $message = "")
        {
            if ($tran_type == "sales") {
                return redirect()->route('carts.index')->with('error', $message);
            } else {
                return redirect()->route('home')->with('error', 'Unknown Request');
            }
        }


        $carts = Carts::where([['created_by', Auth::user()->id], ['id', '=', $request->id]])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return redirectFun($request->tran_type, "Carts not found");
        }

        Carts::where('id', '=', $carts[0]->id)->delete();
        return redirectFun($request->tran_type, "Product Remove from Carts");
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sales_checkout(Request $request)
    {


        $sub_total = $request->sub_total != null ? $request->sub_total : 0;
        $total = $request->total != null ? $request->total : 0;
        $discount = $request->discount != null ? $request->discount : 0;
        $phone_no = $request->phone_no != null ? $request->phone_no : "0";
        $paid = $request->paid != null ? $request->paid : 0;
        $tempSubtotal = 0;


        // Phone number check must be 11 digit, startwith 01, all number is digit
        try {

            if (strlen($phone_no) != 11) {
                return redirect()->route('carts.index')->with('error', 'Phone Number Must be 11 digit');
            } else if ($phone_no[0] != "0" || $phone_no[1] != "1") {
                return redirect()->route('carts.index')->with('error', 'Give a valid Phone Number like (01...)');
            }

            $a = (int) $phone_no; // Chech phone_no in number
            if (strlen(strval($a)) != 10) {
                return redirect()->route('carts.index')->with('error', 'Give a valid Phone Number like (01...)');
            }
        } catch (\Throwable $th) {
            return redirect()->route('carts.index')->with('error', 'Give a valid Phone Number like (01...)');
        }


        $carts = Carts::where([['created_by', Auth::user()->id], ['transaction_type', '=', 'Sales']])->orderBy('id', 'ASC')->get();

        if (count($carts) < 1) {
            return redirect()->route('carts.index')->with('error', 'Cart is empty');
        }

        $temp_qty = 0;

        for ($i = 0; $i < count($carts); $i++) {
            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
                $quantity_as = $carts[$i]->quantity_as;
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
                $quantity_as = $carts[$i]->quantity_as;
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
                $quantity_as = $carts[$i]->quantity_as;
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
                $quantity_as = $carts[$i]->quantity_as;
            }
            $tempSubtotal += $carts[$i]->selling_price * $temp_qty;
        }

        if ($tempSubtotal != $sub_total) {
            return redirect()->route('carts.index')->with('error', 'Something Wrong');
        }


        // update customer Account
        // if customer phone number exit then user it or create new account
        $custome_name = $request->custome_name;
        $customs_list =  Customer::where([['status', 1], ['contact_no', '=', $phone_no]])->get();
        $last_payable = 0;
        $last_receivable = 0;


        $due = $total - $paid;
        if (count($customs_list) == 0) {
            $customer = new Customer();
            $customer->name = strlen($custome_name) == 0 ? "Unknown Name" : $custome_name;
            $customer->contact_no = $phone_no;
            $customer->description =  "";
            $customer->address =  "";
            $customer->receivable = $due > 0 ? $due : 0;
            $customer->created_by = Auth::user()->id;
            $customer->updated_by = Auth::user()->id;
            $customer->save();
        } else {

            $last_payable = $customs_list[0]->payable;
            $last_receivable = $customs_list[0]->receivable;


            // Calculate payable and receivable
            $customer = $customs_list[0];
            if ($due > 0) {
                if ($customer->payable > 0) {
                    if ($customer->payable >= $due) {
                        $customer->payable -= $due;
                    } else {
                        $due -=  $customer->payable;
                        $customer->payable = 0;
                        $customer->receivable += $due;
                    }
                } else {
                    $customer->receivable += $due;
                }
            }

            $customer->save();
        }

         /// Customer Balance History
         if ($due > 0) {
            $customerHistory = new CustomerBalanceHistory();
            $customerHistory->customer_id = $customer->id;
            $customerHistory->last_payable = $last_payable;
            $customerHistory->current_payable = $customer->payable == null ? 0 : $customer->payable;
            $customerHistory->last_receivable = $last_receivable;
            $customerHistory->current_receivable =  $customer->receivable == null ? 0 : $customer->receivable;
            $customerHistory->amount =  $due;
            $customerHistory->transaction_type =  1; // Receivable
            $customerHistory->reason =  "sales to customer";
            $customerHistory->last_update = Carbon::now()->toDateTimeString();
            $customerHistory->updated_by = Auth::user()->id;
            $customerHistory->save();
        }


        //  Create Invoice Tran
        $InvoiceTran = new InvoiceTran();
        $InvoiceTran->customer_id = $customer->id;
        $InvoiceTran->supplier_id = null;
        //$InvoiceTran->supplier_id = $customer->id;
        $InvoiceTran->inv_no = "inv2021";
        $InvoiceTran->tran_type = "sales to customer";
        $InvoiceTran->sub_total = $sub_total;
        $InvoiceTran->paid = $paid;
        $InvoiceTran->due = $total - $paid;
        $InvoiceTran->discount = $discount;
        $InvoiceTran->total = $total;
        $InvoiceTran->created_by = Auth::user()->id;
        $InvoiceTran->updated_by = Auth::user()->id;

        $InvoiceTran->save();
        $invoice_tran_id = $InvoiceTran->id;
        $transaction_type = $InvoiceTran->tran_type;

        //  Create Invoice Tran item

        for ($i = 0; $i < count($carts); $i++) {

            // Qty calculation
            $temp_qty = 0;
            $temp_quantity_as = '';

            if (strtoupper($carts[$i]->quantity_as) == 'TON') {
                $temp_qty = $carts[$i]->quantity * 907.185;
                $temp_quantity_as = 'TON';
            } else if (strtoupper($carts[$i]->quantity_as) == 'BOX') {
                $temp_qty = $carts[$i]->quantity * $carts[$i]->product->quatity_value;
                $temp_quantity_as = 'BOX';
            } else if (strtoupper($carts[$i]->quantity_as) == 'KG') {
                $temp_qty = $carts[$i]->quantity;
                $temp_quantity_as = 'KG';
            } else if (strtoupper($carts[$i]->quantity_as) == 'PCS') {
                $temp_qty = $carts[$i]->quantity;
                $temp_quantity_as = 'PCS';
            }

            $tempSubtotal += $carts[$i]->selling_price *  $temp_qty;

            $InvoiceTranItem = new InvoiceTranItem();
            $InvoiceTranItem->invoice_tran_id =  $invoice_tran_id;
            $InvoiceTranItem->product_id = $carts[$i]->product_id;
            $InvoiceTranItem->tran_type = "sales to customer";
            $InvoiceTranItem->qty =  $temp_qty;
            $InvoiceTranItem->quantity_as =  $temp_quantity_as;
            $InvoiceTranItem->price_per_rate = $carts[$i]->selling_price;
            $InvoiceTranItem->discount = 0;
            $InvoiceTranItem->created_by = Auth::user()->id;
            $InvoiceTranItem->updated_by = Auth::user()->id;

            $InvoiceTranItem->save();

            //update Stock

            if ($carts[$i]->is_allow_advance_sale == 1) {
                $InvoiceTranItem->status = 2; // Processing = 2
                $InvoiceTranItem->is_advance_sales = 1;
                $InvoiceTranItem->save();

                $InvoiceTran->status = 2;  // Processing = 2
                $InvoiceTran->is_advance_sales = 1;
                $InvoiceTran->save();
            } else {
                $stock_details = Stocks::where([['status', '=', 1], ['product_id', $carts[$i]->product_id]])->get();
                if (count($stock_details) > 0) {
                    $stock_details[0]->current_stock = $stock_details[0]->current_stock -  $temp_qty;
                    $stock_details[0]->save();
                }
            }


            // Delete from carts
            Carts::where('id', '=', $carts[$i]->id)->delete();
        }

        // Journal
        $this->common_class_obj->generateJournalFromInvoice($InvoiceTran);

        // dd($invoice_tran_id);

        // ------------- Invoice
        $invoice_details = $this->common_class_obj->generateInvoicePDF($invoice_tran_id);

        return $invoice_details;

        //return redirect()->route('carts.index')->with('success', 'Sales Complate');


    }
}
