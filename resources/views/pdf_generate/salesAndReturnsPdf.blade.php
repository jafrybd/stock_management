@push('css')
@endpush
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
	<title>Invoice &mdash; Stock Management</title>
	<!-- {{-- <link rel="stylesheet" href="./customInvoice.css" /> --}} -->
	<!-- {{-- <link rel="stylesheet" href="{{ asset('./customInvoice.css') }}" /> --}} -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
</head>

<body style="margin: 0px!important;padding: 0px!important;">
	<div class="row">
		<div class="col-lg-12">
			<div class="row">
				<div class="col-6">
					<div class="invoice-title">
						<div class="invoice-number">Generate Invoice</div>
						<div class="invoice-number">Invoice ID: {{ $invoice_details->invoice_id }}</div>
						<div class="invoice-number">Date : {{ Carbon\Carbon::now()->toDayDateTimeString() }}</div>
					</div>
				</div>
				<div class="col-12 text-center">
					<address>
						<h2>Bhtraders.com</h2>
						Kakoli, Banani<br>
						+880123456789<br>
					</address>
				</div>
				<div class="col-12 text-right">
					<img height="100px" src="https://codesign.com.bd/conversations/content/images/2020/03/Sprint-logo-design-Codesign-agency.png" />
				</div>
			</div>

			<hr style="margin-top:-150px;">
			<div class="row">
				<div class="col-md-6">
					<address>
						<strong>Billed From:</strong><br>
						Bhtraders.com<br>
						+880123456789<br>
					</address>
					
				</div>
				<div class="col-md-6 text-right">
					<address>
						<strong>Billed To:</strong><br>
						{{ $invoice_details->name }}<br>
						{{ $invoice_details->phone_no }}<br>
					</address>
				</div>
			</div>

			<div style="margin-top: -100px;">
				<div class="section-title" align='center'>Order Summary</div>
				<!-- <div class="table-responsive"> -->
				<table class="table">
					<tr>
						<!-- <th data-width="40">#</th> -->
						<!-- <th class="text-center">Head</th> -->
						<th class="text-center">Category</th>
						<th class="text-center">Type</th>
						<th class="text-center">Size</th>
						<th class="text-center">Group</th>
						<th class="text-center">Price</th>
						<th class="text-center">Qty</th>
						<th class="text-center">Qty As</th>
						<th class="text-center">Total</th>
					</tr>
					@foreach ($invoice_details as $key => $invoice)
					<tr>
						<!-- <td>{{ $key + 1 }}</td> -->
						<!-- <td class="text-center">{{ $invoice->product_head->title }}</td> -->
						<td class="text-center">@if ($invoice->product->product_category_id == 0)
							None
							@else
							{{ $invoice->product_category->title }}
							@endif
						</td>
						<td class="text-center"> @if ($invoice->product->product_type_id == 0)
							None
							@else
							{{ $invoice->product_type->title }}
							@endif
						</td>

						<td class="text-center">@if ($invoice->product->product_size_id == 0)
							None
							@else
							{{ $invoice->product_size->title }}
							@endif
						</td>
						<td class="text-center">@if ($invoice->product->product_group_id == 0)
							None
							@else
							{{ $invoice->product_group->group_name }}
							@endif
						</td>
						<td class="text-center"> {{ $invoice->price_per_rate }}</td>
						<td class="text-center"> {{ $invoice->qty }}</td>
						<td class="text-center"> {{ $invoice->quantity_as }}</td>
						<td class="text-center"> {{ $invoice->price_per_rate * $invoice->qty }}</td>
					</tr>
					@endforeach

					<div class="flexDiv">
						<div class="col-12">

						</div>
						<div class="col-12 text-right">
							<!-- <div class="flexDiv">
								<div class="col-10 text-right">
									<strong>Subtotal :</strong>
								</div>

								<div class="col-12 text-right">
									<b>{{ $invoice_details->sub_total }}</b>
								</div>
							</div> -->
							<div class="flexDiv">
								<div class="col-10 text-right">
									<strong>Sub Total :</strong><br>
									<strong>Discount :</strong>
									<hr>
									<strong>Total :</strong><br>
									<strong>Paid :</strong><br>
									<strong>Due :</strong><br>
								</div>

								<div class="col-12 text-right">
									<b>{{ $invoice_details->sub_total }}</b><br>
									<b>{{ $invoice_details->discount }}</b>
									<hr>
									<b>{{ $invoice_details->total }}</b><br>
									<b>{{ $invoice_details->paid }}</b><br>
									<b>{{ $invoice_details->due }}</b><br>
								</div>
							</div>
							<!-- <div class="flexDiv">
								<div class="col-10 text-right">
									<strong>Total :</strong>
								</div>

								<div class="col-12 text-right">
									<b>{{ $invoice_details->total }}</b>
								</div>
							</div>
							<div class="flexDiv">
								<div class="col-10 text-right">
									<strong>Paid :</strong>
								</div>
								<div class="col-12 text-right">
									<b>{{ $invoice_details->paid }}</b>
								</div>
							</div>
							<div class="flexDiv">
								<div class="col-10 text-right">
									<strong>Due :</strong>
								</div>
								<div class="col-12 text-right">
									<b>{{ $invoice_details->due }}</b>
								</div>
							</div> -->
						</div>
					</div>
				</table>
			</div>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-12">
			<address>
				<span class="signature">Signature</span>
		</div>
	</div>
	<div class="ft">Copyright @ 2021 BHTraders.Com</div>
	<style>
		.ft {
			display: block;
			margin: auto;
			text-align: center;
			padding: 10px;
			position: fixed;
			height: auto;
			bottom: 0px;
			width: 100%;
			background: #fff;
			color: #000;
		}

		hr {
			margin-bottom: 10px !important;
		}

		strong,
		b {
			margin: 15px !important;
		}

		.signature,
		.title {
			float: left;
		}

		.signature,
		.title {
			margin: 20px 10px;
			border-top: 1px solid #000;
			width: 200px;
			text-align: center;
		}

		.flexDiv {
			width: 100% !important;
			display: flex;
		}

		.invoice .invoice-detail-item .invoice-detail-value {
			font-size: 18px;
			color: #34395e;
			font-weight: 700;
		}

		.invoice .invoice-detail-item .invoice-detail-name {
			letter-spacing: 0.3px;
			color: #08080a;
			margin-bottom: 4px;
		}

		.invoice .invoice-detail-item {
			margin-bottom: 15px;
		}

		.section-title {
			font-size: 18px;
			color: #191d21;
			font-weight: 600;
			position: relative;
			margin: 30px 0 25px 0;
		}

		.invoice {
			box-shadow: 0 4px 8px rgba(0, 0, 0, 0.03);
			background-color: #fff;
			border-radius: 3px;
			border: none;
			position: relative;
			margin-bottom: 30px;
			padding: 40px;
		}

		address {
			margin-bottom: 1rem;
			font-style: normal;
			line-height: inherit;
		}

		.table th {
			border-radius: 10px 0px 10px 0;
			background: black;
			color: white;
			font-size: 13px;
			font-weight: bold;
		}

		.invoice .invoice-title .invoice-number {
			/* float: right; */
			font-size: 20px;
			font-weight: 700;
			margin-top: -45px;
		}

		.table {
			width: 100%;
			margin-bottom: 1rem;
			background-color: transparent;
		}

		table {

			border-collapse: collapse;
			border: 1px solid #fff;
		}

		.table-responsive {
			display: block;
			width: 100%;
			overflow-x: auto;
			-webkit-overflow-scrolling: touch;
			-ms-overflow-style: -ms-autohiding-scrollbar;
		}

		.table-striped tbody tr:nth-of-type(2n+1) {
			background-color: rgba(0, 0, 0, 0.02);
		}

		.table.table-md th,
		.table.table-md td {
			padding: 10px 15px;
		}

		.table.table-md th,
		.table.table-md td {
			padding: 10px 15px;
		}

		.table td,
		.table:not(.table-bordered) th {
			border-top: none;
		}

		.table td,
		.table th {
			/* padding: .75rem; */
			vertical-align: top;
			border-top: 1px solid #dee2e6;
		}

		.table td {
			background: #f0f0f0;
		}
	</style>
</body>

</html>