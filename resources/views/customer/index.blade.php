@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Customer </h1>
            </div>


            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile No</th>
                                        <th>Adddress</th>
                                        <th>Payable</th>
                                        <th>Receivable</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customer_list as $key => $customer)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{ $customer->contact_no }}</td>
                                        <td>{{ $customer->address }}</td>
                                        <td>{{ $customer->payable }}</td>
                                        <td>{{ $customer->receivable }}</td>

                                        <td>
                                            <a class="btn btn-primary edit" href="{{ route('customer.balanceHistory', $customer->id) }}">Balance History</a>

                                            <a href="{{ route('customer.edit', $customer->id) }}" type="button" class="btn btn-primary edit">Edit</a>


                                            <form method="POST" action="{{ route('customer.destroy', $customer->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>

                                            <a class="btn btn-primary edit" onclick="changeModalData( '{{ $customer }}')" data-toggle="modal" data-target="#modal-part2">
                                                Payable/Receivable
                                            </a>
                                        </td>



                                    </tr>
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <form method="POST" action="{{ route('customer.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Customer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <code>*</code></label>
                            <input type="text" name="name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Phone <code>*</code></label>
                            <input type="number" name="phone" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Address <code></code></label>
                            <input type="text" name="address" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Discription <code></code></label>
                            <input type="text" name="discription" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Photo <code></code></label>
                            <input type="file" name="photo" class="form-control form-control-lg" />
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <form method="POST" action="{{ route('customer.payment') }}">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Payable/Receivable</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <code></code></label>
                            <input type="text" name="customer_name" id="customer_name" class="form-control form-control-lg" readonly />
                        </div>
                        <div class="form-group">
                            <select id="transaction_type" name="transaction_type" class="form-control">
                                <option value="0">Payable</option>
                                <option value="1">Receivable</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Amount <code>*</code></label>
                            <input type="number" name="amount" id="amount" class="form-control form-control-lg" />
                        </div>
                        <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                    </div>

                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="editProductHead" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


</div>

@endsection

@section('extra-js')
<script>
    function changeModalData(customer) {
        // console.log(customer);
        customer_data = JSON.parse(customer);

        document.getElementById("id").value = customer_data.id;
        document.getElementById("customer_name").value = customer_data.name;
    }
</script>
@endsection