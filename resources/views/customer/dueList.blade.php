@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Customer Due List</h1>
            </div>


           
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile No</th>
                                        <th>Adddress</th>
                                        <th>Due</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customer_due_list as $key => $customer)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $customer->name }}</td>
                                        <td>{{ $customer->contact_no }}</td>
                                        <td>{{ $customer->address }}</td>
                                        <td>{{ $customer->receivable }}</td>

                                        <td>
                                            <a class="btn btn-primary edit" href="{{ route('customer.balanceHistory', $customer->id) }}">Balance History</a>

                                        </td>



                                    </tr>
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    

</div>

@endsection

@section('extra-js')
<script>
    function changeModalData(customer) {
        // console.log(customer);
        customer_data = JSON.parse(customer);

        document.getElementById("id").value = customer_data.id;
        document.getElementById("customer_name").value = customer_data.name;
    }
</script>
@endsection