<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Invoice for Sales Return</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>

<body>
    <h3>Supplier Name: {{ $invoice_details->supplier_name }}</h3>
    <h3>Phone No: {{ $invoice_details->phone_no }}</h3>
    <h3>Sub Total: {{ $invoice_details->sub_total }}</h3>
    <h3>Discount: {{ $invoice_details->discount }}</h3>
    <h3>Total: {{ $invoice_details->total }}</h3>
    <h3>Paid : {{ $invoice_details->paid }}</h3>
    <h3>Due : {{ $invoice_details->due }}</h3>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Head</th>
                <th>Category</th>
                <th>Type</th>
                <th>Size</th>
                <th>Group</th>
                <th>Price</th>
                <th>Qty</th>
                <th>Qty As</th>
                <th>Total</th>


            </tr>
        </thead>
        <tbody>
            @foreach ($invoice_details as $key => $invoice)
                <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>

                        {{ $invoice->product_head->title }}
                    </td>
                    <td>
                        @if ($invoice->product->product_category_id == 0)
                            None
                        @else
                            {{ $invoice->product_category->title }}
                        @endif
                    </td>
                    <td>
                        @if ($invoice->product->product_type_id == 0)
                            None
                        @else
                            {{ $invoice->product_type->title }}
                        @endif
                    </td>
                    <td>
                        @if ($invoice->product->product_size_id == 0)
                            None
                        @else
                            {{ $invoice->product_size->title }}
                        @endif
                    </td>
                    <td>
                        @if ($invoice->product->product_group_id == 0)
                            None
                        @else
                            {{ $invoice->product_group->group_name }}
                        @endif
                    </td>

                    <td>{{ $invoice->price_per_rate }} </td>
                    <td>{{ $invoice->qty }} </td>
                    <td>{{ $invoice->quantity_as }} </td>
                    <td>{{ $invoice->price_per_rate * $invoice->qty }} </td>
                    {{-- <td>{{ $invoice->total }}</td> --}}


                </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>
