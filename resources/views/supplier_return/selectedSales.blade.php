@extends('layouts.app')
@section('title', 'Product size')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Selected Product to Return</h4>
                    </div>

                    @include('flash-message')

                    <div class="row card-body">
                        <section class="col-8 section">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Head</th>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Group</th>
                                            <th>Price</th>
                                            <th>Qty</th>
                                            <th>Qty As</th>
                                            <th>Total</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($carts as $key => $cart)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>

                                                {{ $cart->product_head->title }}
                                            </td>
                                            <td>
                                                @if ($cart->product->product_category_id == 0)
                                                None
                                                @else
                                                {{ $cart->product_category->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($cart->product->product_type_id == 0)
                                                None
                                                @else
                                                {{ $cart->product_type->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($cart->product->product_size_id == 0)
                                                None
                                                @else
                                                {{ $cart->product_size->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ($cart->product->product_group_id == 0)
                                                None
                                                @else
                                                {{ $cart->product_group->group_name }}
                                                @endif
                                            </td>

                                            <td>{{ $cart->selling_price }} </td>
                                            <td>{{ $cart->quantity }} </td>
                                            <td>{{ $cart->quantity_as }} </td>
                                            <td>{{ $cart->total }}</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <form method="POST" action="{{ route('supplier_return.remove_from_cart', $cart->id) }}" style="display:inline">
                                                            {{ method_field('DELETE') }}
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="id" value="{{$cart->id}}">
                                                            <input type="hidden" name="tran_type" value="Supplier Return">
                                                            <button type="submit" class=" btn btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>-

                                                            </button>
                                                        </form>
                                                    </div>

                                                    <div class="col-6">
                                                        <button class="btn btn-primary" onclick="changeModalData('{{$cart}}')" data-toggle="modal" data-target="#modal-part">
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </section>
                        <div class="col-4">
                            <!-- <div class="form-group row mb-4">
                                <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Invoice ID <code>*</code></label>
                                <div class="col-sm-12 col-md-7">
                                    <input type="text" class="form-control" readonly placeholder="1463341125" />
                                </div>
                            </div> -->

                            <form method="POST" action="{{ route('supplier_return.checkout') }}" id="editForm">

                                {{ method_field('POST') }}
                                {{ csrf_field() }}


                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Supplier Mobile <code>*</code></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="phone_no" id="phone_no" oninput="checkSupplierPhoneNo()" class="form-control" required />
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Supplier Name <code>*</code></label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="supplier_name" id="supplier_name" class="form-control" required />
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Tran Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control selectric" name="tran_type">
                                            <option>Supplier Return</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">SubTotal</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="sub_total" min="0" id="inv_subtotal" class="form-control" readonly placeholder="0" value="{{$subtotal}}" step="any" />
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Discount</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="discount" min="0" id="inv_discount" onkeyup="changeDiscount(event)" class="form-control" placeholder="0" step="any" />
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Total</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="total" min="0" id="inv_total" class="form-control" readonly placeholder="0" value="{{$subtotal}}" step="any" />
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Paid</label>
                                    <div class="col-sm-12 col-md-5">
                                        <input type="number" name="paid" min="0" id="inv_paid" onkeyup="getPaid(event)" class="form-control" placeholder="0" step="any" />
                                    </div>
                                    <div class="col-sm-12 col-md-2">
                                        <button type="button" onclick="fullyPaid()" class="btn btn-success">
                                            Fully Paid
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="
                          col-form-label
                          text-md-right
                          col-12 col-md-3 col-lg-3
                        ">Due</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" id="inv_due" class="form-control" value="{{$subtotal}}" readonly placeholder="0" step="any" />
                                    </div>
                                </div>

                                <div class="row mb-4 justify-content-md-center">
                                    <button type="submit" class="btn btn-primary" style="margin-right: -50px;">
                                        Complete
                                    </button>
                                </div>

                            </form>
                        </div>
                        <!-- </div>
            </div> -->
                    </div>
    </section>
</div>

<!-- Modal -->
{{-- ADD Modal --}}
<div class="modal fade" role="dialog" id="modal-part">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('supplier_return.info_update') }}">
            {{ csrf_field() }}
            <div class=" modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Change Item Info</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {{-- <div class="form-group">
                        <label>Title <code>*</code></label>
                        <input type="text" name="product_name" id="product_name" class="form-control form-control-lg" />
                    </div> --}}
                    <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                    <input type="hidden" name="product_id" id="product_id" class="form-control form-control-lg" readonly />
                    <input type="hidden" name="tran_type" id="tran_type" value="Supplier Return" class="form-control form-control-lg" readonly />

                    <div class="form-group">
                        <label>Qty As <code>*</code></label>
                        <select name="qty_as" id="qty_as" class="form-control form-control-lg">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Qty <code>*</code></label>
                        <input type="number" name="qty" id="qty" class="form-control form-control-lg" step="any" />
                    </div>
                    <div class="form-group">
                        <label>Price <code></code></label>
                        <input type="number" name="price" id="price" class="form-control form-control-lg" />
                    </div>

                </div>
                <div class="modal-footer bg-whitesmoke br">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

@endsection

@section('extra-js')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script>
    function checkSupplierPhoneNo() {

        var phone = document.getElementById("phone_no").value;

        console.log("phone : " + phone);

        if (phone.length == 11) {
            $.ajax({
                url: "{{route('supplier.getSupplierInfoByPhoneNo')}}",
                type: 'POST',
                dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    'phoneNo': phone
                },

                success: function(response) {
                    // console.log("successful");
                    response = JSON.parse(response);

                    // console.log(response);

                    try {
                        if (response.isFindData == true) {
                            document.getElementById("supplier_name").value = response.data.name;
                        }
                    } catch (error) {

                    }

                },
                error: function(xhr, textStatus, errorThrown) {
                    // console.log("Fail");
                    // console.log(textStatus);
                    // console.log(errorThrown);
                    // console.log(xhr);
                    alert("Fail");
                }

            });
        } else if (phone.length > 11) {
            alert("Phone No must be 11 digit");
        }
    };
</script>

<script>
    function changeModalData(cart_obj) {
        console.log(JSON.parse(cart_obj));
        cart_obj = JSON.parse(cart_obj);

        // qty_as

        // if (cart_obj.quantity_as.toUpp)

        $('#qty_as option').remove();

        if ((cart_obj.quantity_as.toUpperCase() == 'KG') || (cart_obj.quantity_as.toUpperCase() == 'TON')) {

            if (cart_obj.quantity_as.toUpperCase() == 'KG') {
                $('#qty_as').append('<option value="KG" selected>KG</option>');
                $('#qty_as').append('<option value="TON">TON</option>');
            } else {
                $('#qty_as').append('<option value="KG">KG</option>');
                $('#qty_as').append('<option value="TON" selected>TON</option>');
            }

        } else {
            if (cart_obj.quantity_as.toUpperCase() == 'PCS') {
                $('#qty_as').append('<option value="PCS" selected>Pcs</option>');
                $('#qty_as').append('<option value="BOX">BOX</option>');
            } else {
                $('#qty_as').append('<option value="PCS" >Pcs</option>');
                $('#qty_as').append('<option value="BOX" selected>BOX</option>');
            }
        }

        // document.getElementById("qty_as").value = cart_obj.quantity_as;
        document.getElementById("qty").value = cart_obj.quantity;
        document.getElementById("price").value = cart_obj.selling_price;
        document.getElementById("product_id").value = cart_obj.product_id;
        document.getElementById("id").value = cart_obj.id;
        document.getElementById("product_name").value = "Test";
    }
</script>

<script>

     (function () {
         console.log("000000000");
        calculateDue();
    })();

    function changeDiscount(event) {
        let subtotal = parseFloat(document.getElementById("inv_subtotal").value);
        let discount = parseFloat(document.getElementById("inv_discount").value);

        subtotal = isNaN(subtotal) ? 0 : subtotal;
        discount = isNaN(discount) ? 0 : discount;

        if (((event.key >= 0 && event.key <= 9) || event.key == '.' || event.key == 'Backspace') && (subtotal - discount) > 0) {
            document.getElementById("inv_total").value = subtotal - discount;
        } else {
            document.getElementById("inv_total").value = subtotal;
            document.getElementById("inv_discount").value = 0;
        }

        calculateDue();

    }

    function getPaid(event) {
        if (!((event.key >= 0 && event.key <= 9) || event.key == '.' || event.key == 'Backspace')) {
            document.getElementById("inv_due").value = document.getElementById("inv_total").value;
            document.getElementById("inv_paid").value = 0;
        }
        calculateDue();
    }

    function paidAmountCheck() {

        paid_value = parseFloat(document.getElementById("inv_paid").value);
        total_value = parseFloat(document.getElementById("inv_total").value);

        paid_value = isNaN(paid_value) ? 0 : paid_value;
        total_value = isNaN(total_value) ? 0 : total_value;

        if (paid_value > total_value) {
            document.getElementById("inv_paid").value = total_value;
            document.getElementById("inv_due").value = 0;
        }
    }

    function calculateDue() {
        let paid = parseFloat(document.getElementById("inv_paid").value);
        let total = parseFloat(document.getElementById("inv_total").value);

        paid = isNaN(paid) ? 0 : paid;
        total = isNaN(total) ? 0 : total;

        paidAmountCheck();


        if ((total - paid) > 0) {
            document.getElementById("inv_due").value = total - paid;
        } else {
            document.getElementById("inv_due").value = 0;
        }

    }

    function fullyPaid() {
        document.getElementById("inv_due").value = "0";
        document.getElementById("inv_paid").value = document.getElementById("inv_total").value;
    }


    function alert1() {
        let data = array()
    }
</script>
@endsection