@extends('layouts.app')
@section('title', 'Products')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Product Add</h1>
                </div>

            </div>
            @if ($errors->any())
            <div class="col-sm-12">
                <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                    @foreach ($errors->all() as $error)
                        <span>
                            <p>{{ $error }}</p>
                        </span>
                    @endforeach
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        @endif

        @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <form method="POST" action="{{ route('product.store') }}" id="editForm">
                                {{ method_field('POST') }}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>Select Head</label>
                                    <select name="product_head" class="form-control" required>
                                        <option value="0" selected="selected">Select Product Head</option>
                                        @foreach ($product_heads as $product_head)
                                            <option value="{{ $product_head->id }}">{{ $product_head->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Category</label>
                                    <select name="product_category" class="form-control">
                                        <option value="0" selected="selected">Select Product Categories</option>
                                        @foreach ($product_categories as $product_cat)
                                            <option value="{{ $product_cat->id }}">{{ $product_cat->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Type</label>
                                    <select name="product_type" class="form-control">
                                        <option value="0" selected="selected">Select Product Type</option>
                                        @foreach ($product_types as $product_typ)
                                            <option value="{{ $product_typ->id }}">{{ $product_typ->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Size</label>
                                    <select name="product_size" class="form-control">
                                        <option value="0" selected="selected">Select Product Size</option>
                                        @foreach ($product_sizes as $size)
                                            <option value="{{ $size->id }}">{{ $size->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Group</label>
                                    <select name="product_group" class="form-control">
                                        <option value="0" selected="selected">Select Product Group</option>
                                        @foreach ($product_groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->group_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Quantity Method</label>
                                    <select name="quantity" id="quantity" class="form-control" onchange="checkQtyChange()" required>
                                        <option selected="selected" value="KG/TON">KG/TON</option>
                                        <option value="Box/Pcs">Box/Pcs</option>
                                    </select>
                                </div>

                                <div id="hidden_div" style="display: none;">Hello hidden content</div>

                                <div class="form-group" id="number_of_quantity_div" style="display:none">
                                  <label>Number of Quantity</label>
                                  <input name="number_of_quantity" type="number" class="form-control" step="any">
                              </div>

                               

                                <div class="card-footer text-right">
                                    <button class="btn btn-primary mr-1" type="submit">Submit</button>

                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <!-- End -->
    </div>



@endsection

@section('extra-js')

    <script type="text/javascript">
        function checkQtyChange() {

            let value = document.getElementById('quantity').value;
            if (value == "Box/Pcs") {
                document.getElementById('number_of_quantity_div').style.display = "block";
            } else {
                document.getElementById('number_of_quantity_div').style.display = "none";
            }
        }
    </script>

@endsection

