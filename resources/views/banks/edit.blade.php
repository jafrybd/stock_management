@extends('layouts.app')
@section('title', 'Products')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1> Edit</h1>
                </div>

            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">

                        <div class="card-body">
                            <form method="POST" action="{{ route('bank.update', $id) }}" id="editForm">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label>User Type</label>
                                    <select name="user_type" id="user_type" class="form-control" onclick="getUserName()"
                                        required>
                                       
                                        <option value="Customer" <?php if ($bankData->user_type == "Customer") {
                                            echo 'selected';
                                        } ?> >Customer</option>
                                        <option value="Supplier" <?php if ($bankData->user_type == "Supplier") {
                                            echo 'selected';
                                        } ?> >Supplier</option>
                                        <option value="Partner" <?php if ($bankData->user_type == "Partner") {
                                            echo 'selected';
                                        } ?> >Partner</option>
                                        <option value="Investor" <?php if ($bankData->user_type == "Investor") {
                                            echo 'selected';
                                        } ?> >Investor</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Select Name</label>
                                    <select name="user_name" id="user_name" class="form-control">

                                        @if($bankData->user_type == "Customer")
                                        <option value={{ $bankData->customer_id }} selected="selected">{{ $bankData->customer->name }}</option>

                                        @elseif($bankData->user_type == "Supplier")
                                        <option value={{ $bankData->supplier_id }} selected="selected">{{ $bankData->supplier->name }}</option>

                                        @elseif($bankData->user_type == "Partner")
                                        <option value={{ $bankData->partner_id }} selected="selected">{{ $bankData->partner->name }}</option>

                                        @elseif($bankData->user_type == "Investor")
                                        <option value={{ $bankData->investor_id }} selected="selected">{{ $bankData->investor->name }}</option>

                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Trasaction Type</label>
                                    <select name="cash_type" class="form-control">
                                        
                                        <option value="1" <?php if ($bankData->cash_type == "1") {
                                            echo 'selected';
                                        } ?> >Payable</option>
                                        <option value="2" <?php if ($bankData->cash_type == "2") {
                                            echo 'selected';
                                        } ?>>Receivable</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Bank Name <code>*</code></label>
                                    <input type="text" name="bank_name" id="bank_name"
                                        class="form-control form-control-lg" value="{{ $bankData->bank_name }}"/>
                                </div>

                                <div class="form-group">
                                    <label>Check No <code>*</code></label>
                                    <input type="text" name="check_no" id="check_no" class="form-control form-control-lg" value="{{ $bankData->check_no }}"/>
                                </div>

                                <div class="form-group">
                                    <label>Amount <code>*</code></label>
                                    <input type="number" name="amount" id="amount" class="form-control form-control-lg" value="{{ $bankData->amount }}"/>
                                </div>
                                <div class="form-group">
                                    <label>Date <code>*</code></label>
                                    <input type="date" name="date" class="form-control form-control-lg" value="{{ $bankData->date }}"/>
                                </div>


                                <input type="hidden" name="id" id="id" value="{{ $bankData->id }}" <div class="card-footer text-right">

                                <div class="card-footer text-right">
                                    <button class="btn btn-primary mr-1" type="submit">Update</button>

                                </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>


        <!-- End -->
    </div>



@endsection

@section('extra-js')

    <script type="text/javascript">
        function getUserName() {

            var user_type = document.getElementById("user_type").value;

            //console.log(user_type);


            $.ajax({
                url: "{{ route('bank.getUserName') }}",
                type: 'POST',
                dataType: "json",
                data: {
                    "_token": "{{ csrf_token() }}",
                    'user_type': user_type
                },

                success: function(response) {
                    //console.log("successful");
                    response = JSON.parse(response);

                    //console.log(response);

                    try {
                        if (response.isFindData == true) {

                            var len = response.data.length;
                            // console.log(len);

                            $("#user_name").empty();
                            for (var i = 0; i < len; i++) {
                                var id = response.data[i]['id'];
                                var name = response.data[i]['name'];

                                $("#user_name").append("<option value='" + id + "'>" + name + "</option>");

                            }
                        }
                    } catch (error) {

                    }

                },
                error: function(xhr, textStatus, errorThrown) {
                    // console.log("Fail");
                    // console.log(textStatus);
                    // console.log(errorThrown);
                    // console.log(xhr);
                    alert("Fail");
                }

            });

        };
    </script>

@endsection
