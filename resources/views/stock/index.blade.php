@extends('layouts.app')
@section('title', 'Products')
    @push('css')


    @endpush

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Stock List</h1>
                </div>
                <div class="col-6 d-flex flex-row-reverse">
                </div>
            </div>

            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Head</th>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Group</th>
                                            <th>Current Stock</th>
                                            <th>Buying Price</th>
                                            <th>Selling Price</th>
                                            <th>Average Price</th>
                                            <th>Last Transaction</th>
                                            <th>Reorder Level</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($stocks as $key => $stock)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td >
                                                    {{ $stock->product_head->title }}
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_category_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_category->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_type_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_type->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_size_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_size->title }}
                                                    @endif
                                                </td>
                                                <td >
                                                    @if ($stock->product->product_group_id == 0)
                                                    None
                                                    @else
                                                    {{ $stock->product_group->group_name }}
                                                    @endif
                                                </td>
                                                <td >{{ $stock->current_stock }}</td>
                                                <td >{{ $stock->buying_price }}</td>
                                                <td >{{ $stock->selling_price }}</td>
                                                <td >{{ $stock->average_price }}</td>
                                                <td >{{ \Carbon\Carbon::parse($stock->last_tran_date)->format('d/m/Y')}}</td>
                                                <td >{{ $stock->reorder_level }}</td>
                                                
                                                

                                                <td>

                                                    <a href="{{ route('stock.edit', $stock->id) }}"
                                                        type="button" class="btn btn-primary edit">Edit</a>

                                                    {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Modal_edit"
                                                    >Edit</button> --}}

                                                    {{-- ///Commented --}}

                                                    {{-- value="{{$stock->id}}" 
                                                    onclick="changeEditProductInfo('<?php echo $stock['id'] ?>',
                                                      '<?php echo $stock['buying_price']; ?>',
                                                       '<?php echo $stock['selling_price']; ?>', '<?php echo $stock['reorder_level']; ?>') " --}}

                                                         


                                                    {{-- <form method="POST"
                                                        action="{{ route('product.destroy', $product->id) }}"
                                                        style="display:inline">
                                                        {{ method_field('DELETE') }}
                                                        {{ csrf_field() }}
                                                        <button type="submit" class=" btn-sm btn-danger"
                                                            onclick="return confirm('Confirm delete?')"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                        </button>
                                                    </form> --}}
                                                </td>



                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- Modal -->
        {{-- ADD Modal --}}
        {{-- <div class="modal fade" tabindex="-1" role="dialog" id="Modal_edit">
            <div class="modal-dialog" role="document">
                Type
                <form method="POST" action="{{ route('stock.price_change') }}">
                    {{ method_field('POST') }}
                    {{ csrf_field() }}
                    <div class=" modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Edit Product Stock</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Buying Price</label>  <code>*</code></label>
                                <input type="number" name="buying_price"  id="buying_price" class="form-control form-control-lg" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Selling Price</label>  <code>*</code></label>
                                <input type="number" name="selling_price" id="selling_price" class="form-control form-control-lg" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Reorder Level</label>  <code>*</code></label>
                                <input type="number" name="reorder_level" id="reorder_level" class="form-control form-control-lg" />
                            </div>
                        </div>
                        <input type="hidden" id="stockId" name="stockId">
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                Close
                            </button>
                            <button type="submit" name="updateProductInfo" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div> --}}


    @endsection

    @section('extra-js')

        <script type="text/javascript">
            function changeEditProductInfo(id, buying_price,selling_price, reorder_Level) {
      
      document.getElementById("selling_price").value = selling_price;
      document.getElementById("stockId").value = id;
      document.getElementById("buying_price").value = buying_price;
      document.getElementById("reorder_level").value = reorder_level;
    }
        </script>
   @endsection
