@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Stock Update</h1>
            </div>

        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">

                    <div class="card-body">
                        <form method="POST" action="{{ route('stock.price_stock_change', $id) }}" id="editForm">
                            {{ method_field('POST') }}
                            {{ csrf_field() }}



                            <div class="form-group">
                                <label>Product Current Stock</label>
                                <input name="current_stock" type="number" value="{{ $stock->current_stock }}" readonly class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Product
                                    New Stock
                                </label>
                                <div class="row">
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <input type="number" min="0" id="newStock" name="new_stock" class="form-control">
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4">
                                        <select id="operator" name="operator" class="form-control">
                                            <option value="add">Add ( + )</option>
                                            <option value="subtract">Subtract ( - )</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label>Buying Price</label>
                                <input name="buying_price" type="number" value="{{ $stock->buying_price }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Selling Price</label>
                                <input name="selling_price" type="number" value="{{ $stock->selling_price }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Average Price</label>
                                <input name="average_price" type="number" value="{{ $stock->average_price }}" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Reorder Level</label>
                                <input name="reorder_level" type="number" value="{{ $stock->reorder_level }}" class="form-control">
                            </div>


                            <input type="hidden" id="stock_id" name="stock_id" value="{{ $stock->id }}">
                            <button class="btn btn-primary mr-1" type="submit">Submit</button>

                    </div>

                </div>
                </form>
            </div>
        </div>
</div>
</section>


<!-- End -->
</div>



@endsection

@section('extra-js')

@endsection