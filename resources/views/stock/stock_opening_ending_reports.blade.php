@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Stock Opening Ending Records</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">


                <div class="card">
                    <div class="card-body p-5">
                        <form method="POST" action="{{ route('stock.stock_opening_ending') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="date" name="search_date" id="search_date" value="{{ $date }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="btn btn-primary" type="submit" onclick="search()" value="Search">
                                    </div>
                                </div>
                            </div>

                        </form>
                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>
                                        <th> Product Head</th>
                                        <th> Product Category</th>
                                        <th> Product Type</th>
                                        <th> Product Size</th>
                                        <th> Opening Group </th>
                                        <th> Opening Amount </th>
                                        <th> Sales Amount </th>
                                        <th> Sales Return Amount</th>
                                        <th> Supplier Receive Amount</th>
                                        <th> Supplier Return Amount</th>
                                        <th> Ending Stock</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    @foreach($list as $key2 => $data)

                                    <tr>
                                        <td> 1 </td>
                                        <td>
                                            @if ($data->product->product_heads == null)
                                                        
                                            @else
                                                    {{ $data->product->product_heads->title }}
                                            @endif
                                        </td>

                                        <td>
                                            @if ($data->product->product_categories == null)
                                                        
                                            @else
                                                    {{ $data->product->product_categories->title }}
                                            @endif
                                        </td>

                                        <td>
                                            @if ($data->product->product_types == null)
                                                        
                                            @else
                                                    {{ $data->product->product_types->title }}
                                            @endif
                                        </td>

                                        <td>
                                            @if ($data->product->product_sizes == null)
                                                        
                                            @else
                                                    {{ $data->product->product_sizes->title }}
                                            @endif
                                        </td>

                                        <td>
                                            @if ($data->product->product_groups == null)
                                                        
                                            @else
                                                    {{ $data->product->product_groups->title }}
                                            @endif
                                        </td>



                                        <td> {{ $data['opening_amount']  }} </td>
                                        <td> {{ $data['sales_amount']  }} </td>
                                        <td> {{ $data['sales_return_amount']  }} </td>
                                        <td> {{ $data['supplier_receive_amount']  }} </td>
                                        <td> {{ $data['supplier_return_amount']  }} </td>
                                        <td> {{ $data['ending_stock']  }} </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>

    </script>

    @endsection