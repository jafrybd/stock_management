<div class="main-sidebar">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="home">Stock Management</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="home">St</a>
    </div>
    <ul class="sidebar-menu">
      <!-- <li class="menu-header">Dashboard</li>
          <li class="nav-item dropdown active">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
            <ul class="dropdown-menu">
              <li><a class="nav-link" href="pages/index-0.html">General Dashboard</a></li>
              <li class="active"><a class="nav-link" href="index.html">Ecommerce Dashboard</a></li>
            </ul>
          </li> -->
      {{-- <li class="menu-header">Product</li> --}}

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cubes"></i> <span>Admin</span></a>
        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('admins.index') }}">Admin List</a>
            
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cubes"></i> <span>Account</span></a>
        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('journal.list') }}">Journal List</a>
            <a class="nav-link" href="{{ route('journal.trial_balance') }}">Trial Balance</a>
            <a class="nav-link" href="{{ route('journal.income_statement') }}">Income Statement</a>
            <a class="nav-link" href="{{ route('journal.owner_equity') }}">Owner Equity</a>
            <a class="nav-link" href="{{ route('journal.balance_sheet') }}">Balance Sheet</a>
          </li>
        </ul>

      </li>


      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-boxes"></i> <span>Product Metarials</span></a>
        <ul class="dropdown-menu">
          <li><a class="nav-link" href="{{ route('product-head.index') }}">Product Head List</a></li>
          <li><a class="nav-link" href="{{ route('product-category.index') }}">Product Category List</a></li>
          <li><a class="nav-link" href="{{ route('product-type.index') }}">Product Type List</a></li>
          <li><a class="nav-link" href="{{ route('product-group.index') }}">Product Group List</a></li>
          <li><a class="nav-link" href="{{ route('product-size.index') }}">Product Size List</a></li>
        </ul>
      </li>
      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-box"></i> <span>Product</span></a>
        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('product.create') }}">Add Product</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('product.index') }}">Product List</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-cubes"></i> <span>Stock</span></a>
        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('stock.index') }}">Stock List</a>
          </li> 
          <li>
            <a class="nav-link" href="{{ route('stock.reOrderList') }}">Reorder Level Product</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('stock.stock_opening_ending') }}">Opening and Ending Stock</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-parachute-box"></i> <span>Suppliers</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('supplier.index') }}">Suppliers List</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-user-tie"></i> <span>Customer</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('customer.index') }}">Customer List</a>
          </li>

          <li>
            <a class="nav-link" href="{{ route('customer.dueList') }}">Customer Due List</a>
          </li>
         
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-handshake"></i> <span>Partner</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('partner.index') }}">Partner List</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-hand-holding-usd"></i> <span>Investor</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('investor.index') }}">Investor List</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-comment-dollar"></i> <span>Expense</span></a>

        {{-- <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('expense.index') }}">Expense List</a>
          </li>
        </ul> --}}

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('general-expense.index') }}">General Expense</a>
          </li>
        </ul>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('regular-expense.index') }}">Regular Expense</a>
          </li>
        </ul>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('total-expense.index') }}">Total Expense</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dolly-flatbed"></i> <span>Invoice List</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('invoice.list') }}">General Invoice</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('invoice.advance_sales_list') }}">Advance Sales Invoice</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dolly-flatbed"></i> <span>Reports</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('reports.date-wise-purchase') }}">Suppliers date wise purchase</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('reports.cash-flow') }}">Cash Flow</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('reports.product-wise-sales-reports-month') }}">Product Wise Sales Report</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-shopping-basket"></i> <span>Sales</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('sales.index') }}">Sale List</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('carts.index') }}">Selected Sales</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-undo-alt"></i> <span>Customer Return</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('customer_return.index') }}">Customer Return List</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('customer_return.selectedCarts') }}">Selected Sales</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-shipping-fast"></i> <span>Product Receive From Supplier</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('supplier_receive.index') }}">Product List</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('supplier_receive.selectedCarts') }}">Selected Product</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dolly-flatbed"></i> <span>Supplier Return</span></a>

        <ul class="dropdown-menu">
          <li>
            <a class="nav-link" href="{{ route('supplier_return.index') }}">Product List</a>
          </li>
          <li>
            <a class="nav-link" href="{{ route('supplier_return.selectedCarts') }}">Selected Product</a>
          </li>
        </ul>

      </li>

      <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown" data-toggle="dropdown"><i class="fas fa-dolly-flatbed"></i> <span>Banks</span></a>

        <ul class="dropdown-menu">

          <li>
            <a class="nav-link" href="{{ route('bank.index') }}">Bank Transactions</a>
          </li>
          
        </ul>

      </li>

  </aside>

</div>