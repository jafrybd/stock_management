@extends('layouts.app')
@section('title', 'Product Head')
@push('css')


@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Regular Expense</h1>
            </div>


            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">

                        <div class="owl-carousel owl-theme" id="products-carousel">

                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="filter_month">
                                            <option value="">Select Month</option>
                                            @foreach ($months as $key => $month)
                                            <option value="{{ $key }}">{{ $month }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <select class="form-control selectric" id="filter_year">

                                            <option value="">Select Year</option>
                                            <?php
                                            for ($year = 2015; $year <= 2050; $year++) {
                                                $selected = isset($getYear) && $getYear == $year ? 'selected' : '';
                                                echo "<option value=$year $selected>$year</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>


                                <input type="button" id="filter" value="Search">
                            </div>

                        </div>


                        <div class="table-responsive">
                            <table id="example2" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>Month</th>
                                        <th>Year</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($expenses as $key => $expense)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $expense->id }}">{{ $expense->title }}</td>
                                        <td id=>{{ $expense->amount }}</td>
                                        <td id=>{{ $expense->date }}</td>
                                        <td id=>{{ \Carbon\Carbon::parse($expense->date)->format('F') }}</td>
                                        <td id=>{{ \Carbon\Carbon::parse($expense->date)->format('Y') }}</td>

                                        <td>

                                            <a href="{{ route('regular-expense.edit', $expense->id) }}" type="button" class="btn btn-primary edit">Edit</a>


                                            <form method="POST" action="{{ route('regular-expense.destroy', $expense->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                        </td>



                                    </tr>
                                    @endforeach



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <form method="POST" action="{{ route('regular-expense.store') }}">
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Regular Expense</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title <code>*</code></label>
                            <input type="text" name="title" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Amount <code>*</code></label>
                            <input type="number" name="amount" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Date <code>*</code></label>
                            <input type="date" name="date" class="form-control form-control-lg" />
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

@endsection

@section('extra-js')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> -->
<script>
    $(document).on('click', '#filter', function(e) {

        var filter_month = document.getElementById("filter_month").value;
        var filter_year = document.getElementById("filter_year").value;


        $.ajax({
            url: "{{route('regular-expense.search')}}",
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                'filter_month': filter_month,
                'filter_year': filter_year
            },


            success: function(response) {

                try {
                    response = JSON.parse(response);

                    if (response.success == false) {
                        alert(response.message);
                    } else {
                        // alert(response.message);
                    }
                } catch (error) {
                    $('#example2 tbody').remove();
                    $('#example2').append(response);
                }


            },
            error: function(xhr, textStatus, errorThrown) {

                // console.log("Fail");
                console.log(textStatus);
                console.log(errorThrown);
                console.log(xhr);
                alert("Fail");
            }

        });
    });
</script>
@endsection