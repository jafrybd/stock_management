@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Total Expense</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <form method="POST" action="{{ route('total-expense.index') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col-4 mb-4">
                        </div>

                        <div class="form-group col-4 mb-4">
                            <!-- <div class="col-sm-12 col-md-12"> -->
                            <div class="input-group date" data-provide="datepicker">
                                <input type="text" class="form-control" value="{{ $todaysMonthYear }}" name="datepicker" id="datepicker" />


                            </div>
                            <!-- </div> -->
                        </div>

                        <input type="submit" onclick="search()" value="Search">
                    </div>

                </form>

                <div class="card">
                    <div class="card-body p-5">

                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Expense Type</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    @foreach($list as $key2 => $data)

                                    <tr>
                                        <td> {{ $key2 + 1  }} </td>
                                        <td> {{ $data['title']  }} </td>
                                        <td> {{ $data['amount']  }} </td>
                                        <td> {{ $data['expense_type']  }} </td>
                                    </tr>

                                    @endforeach

                                    <tr>
                                        <td>  </td>
                                        <td> Total </td>
                                        <td> {{ $totalAmount  }} Tk</td>
                                        <td> </td>
                                    </tr>

                                    

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $("#datepicker").datepicker({
            format: "MM, yyyy",
            startView: "months",
            minViewMode: "months"
        });
    </script>

    @endsection