<tbody>
    @foreach ($expenses as $key => $expense)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td id="t{{ $expense->id }}">{{ $expense->title }}</td>
            <td id=>{{ $expense->amount }}</td>
            <td id=>{{ $expense->date }}</td>
            <td id=>{{ \Carbon\Carbon::parse($expense->date)->format('F') }}</td>
            <td id=>{{ \Carbon\Carbon::parse($expense->date)->format('Y') }}</td>

            <td>

                <a href="{{ route('regular-expense.edit', $expense->id) }}" type="button"
                    class="btn btn-primary edit">Edit</a>


                <form method="POST" action="{{ route('regular-expense.destroy', $expense->id) }}"
                    style="display:inline">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i
                            class="fa fa-trash-o" aria-hidden="true"></i>Delete

                    </button>
                </form>
            </td>



        </tr>
    @endforeach
