@extends('layouts.app')


@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Invoice Details</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
                @if ( $invoice_tran_list[0]->is_advance_sales == 1 )

                <p style="color: green; padding-left: 20px;">Advance Salse</p>

                @if ( $invoice_tran_list[0]->status == 2 )
                <form method="POST" action="{{ route('invoice.complete_advance_sales') }}">
                    @csrf
                    <input type="submit" class="btn btn-primary" value="Complete Invoice">
                    <input type="hidden" name="id" value="{{ $invoice_tran_list[0]->id }}">

                </form>
                @endif

                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    @include('flash-message')

                    <div class="row card-body">
                        <div class="col-8 section">
                            <br> <br>
                            <h4> <small>Inv No:</small> {{ $invoice_tran_list[0]->inv_no }}</h4>
                            <h4> <small>Tran Type:</small> {{ strtoupper( $invoice_tran_list[0]->tran_type) }}</h4>

                            @if ($invoice_tran_list[0]->tran_type == "sales to customer" || $invoice_tran_list[0]->tran_type == "Customer Return" )
                            <h4> <small>Customer:</small> <strong>{{ $invoice_tran_list[0]->customer->name }}</strong> <small> ({{ $invoice_tran_list[0]->customer->contact_no }})</small></h4>
                            @else
                            <h4> <small>Supplier:</small> <strong>{{ $invoice_tran_list[0]->supplier->name }}</strong> <small>({{ $invoice_tran_list[0]->supplier->contact_no }})</small></h4>
                            @endif

                        </div>
                        <div class="col-4 section">
                            Date: {{ $invoice_tran_list[0]->created_at }} <br> <br>
                            <h4> <small>Sub Total:</small> {{ $invoice_tran_list[0]->sub_total }} <small>Tk</small> </h4>
                            <h4> <small>Discount:</small> {{ $invoice_tran_list[0]->discount }} <small>Tk</small></h4>
                            <h4> <small>Total:</small> {{ $invoice_tran_list[0]->total }} <small>Tk</small> </h4>
                            <h4> <small>Due:</small> {{ $invoice_tran_list[0]->due }} <small>Tk</small> </h4>
                            <h4> <small>Paid:</small> {{ $invoice_tran_list[0]->paid }} <small>Tk</small> </h4>

                        </div>
                        <section class="col-12 section">
                            <br> <br>
                            <div class="table-responsive">
                                <table id="" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Head</th>
                                            <th>Category</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Group</th>
                                            <th>Qty</th>
                                            <th>Qty As</th>
                                            <th>Price Unit</th>
                                            <th>Discount</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($invoice_tran_item_list as $key => $invoice_tran_item)
                                        <tr>
                                            <td>{{ $key + 1 }}
                                                @if ($invoice_tran_item->is_advance_sales == 1 && $invoice_tran_list[0]->status == 2)
                                                <p style="color:red">(Advance Sale) </p>
                                                @endif
                                            </td>
                                            <td>
                                                {{ $invoice_tran_item->product->product_heads->title }}
                                            </td>
                                            <td>
                                                @if (is_null($invoice_tran_item->product->product_category_id))
                                                None
                                                @else
                                                {{ $invoice_tran_item->product->product_categories->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (is_null($invoice_tran_item->product->product_type_id))
                                                None
                                                @else
                                                {{ $invoice_tran_item->product->product_types->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if ( is_null($invoice_tran_item->product->product_size_id))
                                                None
                                                @else
                                                {{ $invoice_tran_item->product->product_sizes->title }}
                                                @endif
                                            </td>
                                            <td>
                                                @if (is_null($invoice_tran_item->product->product_group_id))
                                                None
                                                @else
                                                {{ $invoice_tran_item->product->product_groups->group_name }}
                                                @endif
                                            </td>

                                            <td>{{ $invoice_tran_item->qty }} </td>
                                            <td>{{ $invoice_tran_item->quantity_as }} </td>
                                            <td>{{ $invoice_tran_item->price_per_rate }} </td>
                                            <td>{{ $invoice_tran_item->discount }} </td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </section>
                    </div>
    </section>
</div>


@endsection
