<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF Demo in Laravel 7</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  </head>
  <body>
    <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Name</th>
        <th>Mobile No</th>
        <th>Balance</th>
        <th>Percentis</th>
        <th>Adddress</th>
       
    </tr>
      </thead>
      <tbody>
        @foreach ($partner as $key => $partner)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $partner->id }}">{{ $partner->name }}</td>
                                        <td>{{ $partner->contact_no }}</td>
                                        <td>{{ $partner->balance }}</td>
                                        <td>{{ $partner->percentage }}</td>
                                        <td>{{ $partner->address }}</td>


                                    </tr>
                                    @endforeach
      </tbody>
    </table>
  </body>
</html>