@extends('layouts.app')
@section('title', 'Product Head')
    @push('css')


    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Balance History </h1>
                    <br>
                    <h1>Name : {{ $partner_name }}</h1>
                    <br>
                    <h1>Current Balance : {{ $current_balance }}</h1>
                </div>
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table id="example" class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Last Balance</th>
                                            <th>Add / Sutract</th>
                                            <th>Payable / Receivable</th>
                                            <th>Current Balance </th>
                                            <th>Updated On</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($partner_balance_history as $key => $balance)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>

                                                <td>{{ $balance->previous_balance }}</td>
                                                <td>
                                                    @if ($balance->current_balance > $balance->previous_balance)
                                                        +{{ abs($balance->current_balance - $balance->previous_balance) }}
                                                    @elseif($balance->current_balance < $balance->previous_balance)
                                                            -{{ abs($balance->current_balance - $balance->previous_balance) }}
                                                    @endif
                                                </td>

                                                <td>
                                                    @if ($balance->current_balance > $balance->previous_balance)
                                                       Payable
                                                    @elseif($balance->current_balance < $balance->previous_balance)
                                                            Receivable
                                                    @endif
                                                </td>
                                                <td>{{ $balance->current_balance }}</td>
                                                <td>{{ \Carbon\Carbon::parse($balance->last_update)->format('d/m/Y') }}
                                                </td>

                                            </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>


    </div>

@endsection

@section('extra-js')
<script>
    function changeModalData(investor) {
        console.log(investor);

        investor_data = JSON.parse(investor);

        document.getElementById("id").value = investor_data.id;
        document.getElementById("current_balance").value = investor_data.balance;
        //console.log(product_obj.id);

    }
</script>
@endsection
