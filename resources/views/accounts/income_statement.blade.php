@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Income Statement </h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">

                <div class="card">
                    <div class="card-body p-5">
                        <div class="row">
                            <div class="col-lg-6 table-responsive p-5">
                                <h1># Sales Income</h1>
                                <div class="table-responsive">
                                    <table id="example33" class="table table-striped">
                                        <thead>
                                            <tr class="thh2">
                                                <th>#</th>
                                                <th>Details A/C</th>
                                                <th>Amount TK</th>
                                            </tr>
                                        </thead>
                                        <tbody class="thh2">
                                            <?php $sales_total = 0; ?>
                                            @foreach ($incomeStatementEntityData['Sales Income'] as $key => $entityData)
                                            <tr>
                                                <td> {{$key + 1 }} </td>

                                                @if( $entityData['cal'] == "+" )

                                                <td> {{ $entityData['name'] }} </td>
                                                <?php $sales_total += $entityData['amount']; ?>

                                                @else

                                                <td> <b>(-)</b> {{ $entityData['name'] }} </td>
                                                <?php $sales_total -= $entityData['amount']; ?>

                                                @endif

                                                <td> {{ $entityData['amount'] }} </td>
                                            </tr>
                                            @endforeach
                                            <tr class="thh2">
                                                <td class="totalNew"> </td>
                                                <td class="totalNew"> <b> Total </b> </td>

                                                <td class="totalNew"> <b>{{ $sales_total}}</b> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <div class="col-lg-6 table-responsive p-5">
                                <h1># Cost Of Goods Sold</h1>
                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr class="thh2">

                                            <th>#</th>
                                            <th>Details A/C</th>
                                            <th>Amount TK</th>
                                        </tr>
                                    </thead>
                                    <tbody class="thh2">
                                        <?php $cost_of_goods_sold = 0; ?>

                                        @foreach ($incomeStatementEntityData['Cost Of Goods Sold'] as $key => $entityData)

                                        <tr>
                                            <td> {{$key + 1 }} </td>

                                            @if( $entityData['cal'] == "+" )

                                            <td> {{ $entityData['name'] }} </td>
                                            <?php $cost_of_goods_sold += $entityData['amount']; ?>

                                            @else

                                            <td> <b>(-)</b> {{ $entityData['name'] }} </td>
                                            <?php $cost_of_goods_sold -= $entityData['amount']; ?>

                                            @endif

                                            <td> {{ $entityData['amount'] }} </td>
                                        </tr>

                                        @endforeach

                                        <tr class="thh2">
                                            <td class="totalNew"> </td>
                                            <td class="totalNew"> <b> Total </b> </td>

                                            <td class="totalNew"> {{ $cost_of_goods_sold}} </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>

                            <div class="col-lg-6 table-responsive p-5">
                                <h1># Expense</h1>

                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr class="thh2">
                                            <th>#</th>
                                            <th>Details A/C</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody class="thh2">
                                        <?php $expense_total = 0; ?>
                                        @foreach ($incomeStatementEntityData['Expense'] as $key => $entityData)
                                        <tr>
                                            <td> {{$key + 1 }} </td>

                                            @if( $entityData['cal'] == "+" )

                                            <td> {{ $entityData['name'] }} </td>
                                            <?php $expense_total += $entityData['amount']; ?>

                                            @else

                                            <td> <b>(-)</b> {{ $entityData['name'] }} </td>
                                            <?php $expense_total -= $entityData['amount']; ?>

                                            @endif

                                            <td> {{ $entityData['amount'] }} </td>
                                        </tr>
                                        @endforeach


                                        <tr class="thh2">
                                            <td class="totalNew"></td>
                                            <td class="totalNew"> <b> Total </b> </td>
                                            <td class="totalNew"> <b>{{ $expense_total}}</b> </td>
                                        </tr>

                                    </tbody>
                                </table>
                            </div>


                            <div class="col-lg-6 table-responsive p-5">
                                <h1> Final Income </h1>
                                <table id="example33" class="table table-striped">
                                    <thead>
                                        <tr class="thh2">
                                            <th>Title</th>
                                            <th>Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody class="thh2">
                                        <tr>
                                            <td>Sales total</td>
                                            <td>{{ $sales_total}}</td>
                                        </tr>
                                        <tr>
                                            <td>Cost of goods sold</td>
                                            <td>{{ $cost_of_goods_sold}}</td>
                                        </tr>
                                        <tr>
                                            <td>Expense total</td>
                                            <td>{{ $expense_total}}</td>
                                        </tr>
                                        <tr class="thh2">
                                            <td class="totalNew">Total</td>
                                            <td class="totalNew">{{ $sales_total -  $cost_of_goods_sold -  $expense_total}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <!-- <code>Income = sales total - cost of goods sold - expense total</code> -->
                                <!-- <h5>Income = sales total - cost of goods sold - expense total </h5> -->
                                <!-- <h5>Income = ({{ $sales_total}} - {{ $cost_of_goods_sold}} - {{ $expense_total}}) Tk</h5>
                                <h5>Income = {{ $sales_total -  $cost_of_goods_sold -  $expense_total}} Tk</h5> -->
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')

    <script>
        // $(document).ready(function() {
        //     var table = $('#example').DataTable({
        //         "pageLength": 25
        //     });
        // });
    </script>
    @endsection