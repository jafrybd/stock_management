@extends('layouts.app')
@section('title', 'Products')
@push('css')


@endpush

@section('content')
<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Product Wise Sales Reports Month</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-3">
                        <form method="POST" action="{{ route('reports.cash-flow') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="input-group date" data-provide="datepicker">
                                            <input type="date" class="form-control" value="{{ $todaysMonthYear }}" name="datepicker" id="datepicker" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="btn btn-primary" type="submit" onclick="search()" value="Search">
                                    </div>
                                </div>
                            </div>

                        </form>
                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>
                                        <th>Description</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    @foreach($list as $key2 => $data)

                                    <tr>
                                        <td> {{ $key2 + 1  }} </td>
                                        <td> {{ $data->description  }} </td>
                                        <td> {{ $data->type  }} </td>
                                        <td> {{ $data->amount  }} </td>
                                        <td> {{ $data->created_at  }} </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        $("#datepicker").datepicker({
            format: "MM, yyyy",
            startView: "months",
            minViewMode: "months"
        });
    </script> -->

    @endsection