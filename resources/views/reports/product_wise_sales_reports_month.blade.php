@extends('layouts.app')
@section('title', 'Products')
@push('css')
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.min.css" rel="stylesheet"> -->
@endpush

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">
        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Product Wise Sales Reports Month</h1>
            </div>
            <div class="col-6 d-flex flex-row-reverse">
            </div>
        </div>

        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">

                <div class="card">
                    <div class="card-body p-5">
                        <form method="POST" action="{{ route('reports.product-wise-sales-reports-month') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="row">


                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="input-group date" data-provide="datepicker">

                                            <input type="date" class="date form-control" value="{{ $todaysMonthYear }}" id="datepicker" name="datepicker" />
                                            <!-- <input type="text" class="form-control" value="{{ $todaysMonthYear }}" id="datepicker" name="datepicker" /> -->

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group col-4 mb-4">
                                    <div class="col-sm-12 col-md-12">
                                        <input class="btn btn-primary" type="submit" onclick="search()" value="Search">
                                    </div>
                                </div>
                            </div>

                        </form>

                        <div class="table-responsive">
                            <table id="example33" class="table table-striped">
                                <thead>
                                    <tr class="thh2">
                                        <th>#</th>

                                        <th>Head</th>
                                        <th>Category</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                        <th>Group</th>

                                        <th>Product Id</th>
                                        <th>Qty</th>
                                        <th>Qty Type</th>
                                    </tr>
                                </thead>
                                <tbody class="thh2">
                                    @foreach($list as $key2 => $data)

                                    <tr>
                                        <td> 1 </td>

                                        <td>
                                            {{ $data['product_details']->product_heads->title }}
                                        </td>
                                        <td>
                                            @if ($data['product_details']->product_category_id == 0)
                                            None
                                            @else
                                            {{ $data['product_details']->product_categories->title }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($data['product_details']->product_type_id == 0)
                                            None
                                            @else
                                            {{ $data['product_details']->product_types->title }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($data['product_details']->product_size_id == 0)
                                            None
                                            @else
                                            {{ $data['product_details']->product_sizes->title }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($data['product_details']->product_group_id == 0)
                                            None
                                            @else
                                            {{ $data['product_details']->product_groups->group_name }}
                                            @endif
                                        </td>


                                        <td> {{ $data['product_id']  }} </td>
                                        <td> {{ $data['qty_total']  }} </td>
                                        <td> {{ $data['quantity_as']  }} </td>
                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    @endsection

    @section('extra-js')
    <!-- <script

              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/css/datepicker.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.2.0/js/bootstrap-datepicker.min.js"></script>
    <script>
        // $("#datepicker").datepicker({
        //     format: "MM, yyyy",
        //     startView: "months",
        //     minViewMode: "months"
        // });  
        $("#datepicker").datepicker({
            format: "d MM,y",
        });
    </script>-->
    <script>
        $('#customDate').datepicker({
            format: "MM dd, yyyy",
            autoclose: true
        });
    </script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"> </script> -->
    @endsection