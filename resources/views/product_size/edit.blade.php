@extends('layouts.app')
@section('title', 'Product size')
    @push('css')
        
        
    @endpush

@section('content')

    <!-- Main Content -->
    <div class="main-content">
        <section class="section">

            <div class="col-12 section-header">
                <div class="col-6">
                    <h1>Product Size</h1>
                </div>


                
            </div>
            @if ($errors->any())
                <div class="col-sm-12">
                    <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <span>
                                <p>{{ $error }}</p>
                            </span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif

            @include('flash-message')

            <div class="section-body">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body p-0">
                            <form method="POST" action="{{route('product-size.update',$id)}}" id="editForm">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
                                <div class=" modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Edit Product Size</h5>
                                        
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Title <code>*</code></label>
                                            <input type="text" name="title" id="title" value="{{ $product_size->title }}" class="form-control form-control-lg"  />
                                        </div>
                                    </div>
                                    <input type="hidden" name="id" id="id" value="{{ $product_size->id }}" class="form-control form-control-lg"  />
                                    <div class="modal-footer bg-whitesmoke br">
                                        
                                        <button type="submit" class="btn btn-primary">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <!-- Modal -->
            {{-- ADD Modal --}}
        
    </div>

@endsection

@section('extra-js')

@endsection
