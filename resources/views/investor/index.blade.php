@extends('layouts.app')

@section('content')

<!-- Main Content -->
<div class="main-content">
    <section class="section">

        <div class="col-12 section-header">
            <div class="col-6">
                <h1>Investor List </h1>
            </div>


            <div class="col-6 d-flex flex-row-reverse">
                <button class="btn btn-primary" data-toggle="modal" data-target="#modal-part">
                    + Add New
                </button>
            </div>
        </div>
        @if ($errors->any())
        <div class="col-sm-12">
            <div class="alert  alert-warning alert-dismissible fade show" role="alert">
                @foreach ($errors->all() as $error)
                <span>
                    <p>{{ $error }}</p>
                </span>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif

        @include('flash-message')

        <div class="section-body">
            <div class="col-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Mobile No</th>
                                        <th>Balance</th>
                                        <th>Percentis</th>
                                        <th>Adddress</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($investor_list as $key => $investor)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td id="t{{ $investor->id }}">{{ $investor->name }}</td>
                                        <td>{{ $investor->contact_no }}</td>
                                        <td>{{ $investor->balance }}</td>
                                        <td>{{ $investor->percentage }}</td>
                                        <td>{{ $investor->address }}</td>

                                        <td>

                                            <a href="{{ route('investor.edit', $investor->id) }}" type="button" class="btn btn-primary edit">Edit</a>
                                            <a class="btn btn-primary edit" onclick="changeModalData( '{{ $investor }}')" data-toggle="modal" data-target="#modal-part2">
                                                Update Money
                                            </a>
                                            <a href="{{ route('investor.balanceHistory', $investor->id) }}" type="button" class="btn btn-primary edit">
                                                Balance History
                                            </a>

                                            <form method="POST" action="{{ route('investor.destroy', $investor->id) }}" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class=" btn-sm btn-danger" onclick="return confirm('Confirm delete?')"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete

                                                </button>
                                            </form>
                                        </td>

                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

    <!-- Modal -->
    {{-- ADD Modal --}}
    <div class="modal fade" tabindex="-1" role="dialog" id="modal-part">
        <div class="modal-dialog" role="document">
            {{-- {{route('category.store')}} --}}
            <form method="POST" action="{{ route('investor.store') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Investor</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name <code>*</code></label>
                            <input type="text" name="name" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Phone <code>*</code></label>
                            <input type="number" name="phone" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Percentage(%) <code>*</code></label>
                            <input type="number" name="percentile" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Address <code></code></label>
                            <input type="text" name="address" class="form-control form-control-lg" />
                        </div>
                        <div class="form-group">
                            <label>Photo <code></code></label>
                            <input type="file" name="photo" class="form-control form-control-lg" />
                        </div>
                    </div>
                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modal-part2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">

            <form method="POST" action="{{ route('investor.moneyUpdate') }}">
                {{ method_field('POST') }}
                {{ csrf_field() }}
                <div class=" modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Update Money</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Current Balance <code>*</code></label>
                            <input type="text" name="current_balance" id="current_balance" class="form-control form-control-lg" readonly />
                        </div>
                        <div class="form-group">
                            <label>Money
                            </label>
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <input type="number" min="1" id="newStock" name="money" class="form-control">
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="operator" name="operator" class="form-control">
                                    <option value="add">+</option>
                                    <option value="subtract">-</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" class="form-control form-control-lg" readonly />
                    </div>

                    <div class="modal-footer bg-whitesmoke br">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                            Close
                        </button>
                        <button type="submit" id="editProductHead" class="btn btn-primary">
                            Update Balance
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('extra-js')
<script>
    function changeModalData(investor) {
        console.log(investor);

        investor_data = JSON.parse(investor);

        document.getElementById("id").value = investor_data.id;
        document.getElementById("current_balance").value = investor_data.balance;
        //console.log(product_obj.id);

    }
</script>

@endsection